4524	MTHFR	5,10-methylenetetrahydrofolate reductase (NADPH)	1p36.3
5999	RGS4	regulator of G-protein signaling 4	1q23.3
5362	PLXNA2	plexin A2	1q32.2
27185	DISC1	disrupted in schizophrenia 1	1q42.1
7166	TPH1	tryptophan hydroxylase 1	11p15.3-p14
1815	DRD4	dopamine receptor D4	11p15.5
2900	GRIK4	glutamate receptor, ionotropic, kainate 4	11q22.3
1813	DRD2	dopamine receptor D2	11q23
