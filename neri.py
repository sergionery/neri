# -*- coding: utf-8 -*-
import sys
import os

def start_interface():
    os.system("python system/neri_GUI.py")

def start_in_terminal(args):
    import system.neri_CLI as terminal
    terminal.main(args)

if __name__ == '__main__':
    if(len(sys.argv) == 2):
        start_in_terminal(sys.argv[1])
    else:
        start_interface()

