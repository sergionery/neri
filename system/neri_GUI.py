from kivy.app import App
from kivy.uix.tabbedpanel import TabbedPanelHeader, TabbedPanelItem
from kivy.lang import Builder
from kivy.core.window import Window
from kivy.uix.dropdown import DropDown
from kivy.config import Config

# ConfigParser For Python2.7 or Python3
import sys
if sys.version_info[0] == 3:
    import configparser
else:
    import ConfigParser as configparser

# INI - TELA NOVA - PREPROCESSAMENTO
#from GUI.py_files.Preprocess import *
# FIM - TELA NOVA - PREPROCESSAMENTO

from GUI.py_files.NewExperiment import *
from GUI.py_files.Input import *
from GUI.py_files.Analysis import *
from GUI.py_files.Execute import *
from GUI.py_files.MenuHelp import *
from GUI.py_files.Load import *

# For Windows (disabling default full screen)
import platform
if platform.system() == 'Windows':
    Window.fullscreen = False

Builder.load_string(
'''
<RootWidget>
    TabbedPanel:
        id: main
        do_default_tab: True
'''
)

class RootWidget(FloatLayout):
    def run_button(self, execute):
        self.ids.main.switch_to(execute)

    def create_button(self, load):
        self.ids.main.switch_to(load)

    def generate_button(self, new):
        self.ids.main.switch_to(new)        

    def __init__(self,  **kwargs):
        super(RootWidget, self).__init__(**kwargs)
        #self.ids['main'].default_tab_text = 'Preprocess'
        self.ids['main'].default_tab_text = 'New'
        #new = TabbedPanelItem(text='New')
        load = TabbedPanelItem(text='Load')
        execute = TabbedPanelItem(text='Execute')
        analysis = TabbedPanelItem(text='Analysis')
        help = TabbedPanelItem(text='Help')

        #self.ids['main'].add_widget(new)
        self.ids['main'].add_widget(load)
        self.ids['main'].add_widget(execute)
        self.ids['main'].add_widget(analysis)
        self.ids['main'].add_widget(help)

        #self.ids['main'].default_tab_content = Preprocess()

        self.ids['main'].default_tab_content = NewExperiment()
        #new.content = NewExperiment()
        load.content = Load()
        analysis.content = Analysis()
        execute.content = Execute()
        help.content = MenuHelp()


        if self.ids['main'].default_tab_content.ids['experiment_input'].text == '' or \
           self.ids['main'].default_tab_content.ids['expression_input'].text == '' or \
           self.ids['main'].default_tab_content.ids['seeds_input'].text == '':
            pass
        else:            
            self.ids['main'].default_tab_content.ids['create_button'].bind(on_press=lambda x:self.run_button(load))
            #new.content.ids['create_button'].bind(on_press=lambda x:self.run_button(load))
            load.content.ids['run_button'].bind(on_press=lambda x:self.run_button(execute))
        #self.ids['main'].default_tab_content.ids['generate_button'].bind(on_press=lambda x:self.generate_button(load))


        load.content.ids["next_button_load"].bind(on_press=lambda x:self.run_button(execute))
        execute.content.ids["bar"].bind(on_press=lambda x:self.run_button(analysis))
        execute.content.ids["previous_exe"].bind(on_press=lambda x:self.run_button(load))



class MainApp(App):
    def build(self):
        return RootWidget()

if __name__ == '__main__':
    MainApp().run()

