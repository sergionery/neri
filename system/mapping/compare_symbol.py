# -*- coding: utf-8 -*-

# compare symbols field with authority symbols field
filename = 'Homo_sapiens.gene_info-OLD'

f = open(filename, 'r')
# reads the first line (header)
line = f.readline()
#
j = 0
for i,line in enumerate(f):
    lst = line.split('\t')
    # reads fields
    symbol    = lst[2]
    authority = lst[10]
    if symbol != authority:
        j+=1
        print j, i, symbol, authority
f.close()
