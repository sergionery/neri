# -*- coding: utf-8 -*-

from kivy.lang import Builder
from kivy.uix.floatlayout import FloatLayout

Builder.load_file('GUI/kv_files/help.kv')

class Help(FloatLayout):

    '''read text from "help.txt" and copy it in the label "help" from "help.kv" '''
    def Text_Label(self):
        filename = 'system/help/help.txt'
        fl = open(filename, 'rt')
        lst = fl.readlines()
        fl.close()

        text = ''

        for i in lst:
            text += i

        self.ids.help.text = text

    def __init__(self, **kwargs):
        super(Help, self).__init__(**kwargs)
        self.Text_Label()
