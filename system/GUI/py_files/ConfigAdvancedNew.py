# -*- coding: utf-8 -*-

from kivy.uix.floatlayout import FloatLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.properties import ObjectProperty
from kivy.uix.screenmanager import Screen
from kivy.uix.button import Button
from kivy.uix.dropdown import DropDown
from kivy.clock import Clock
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.textinput import TextInput
from GUI.py_files.Popup_help import *
from GUI.py_files.Hint import *
from GUI.py_files.filebrowser import FileBrowser
from os.path import sep, expanduser, isdir, dirname


from kivy.lang import Builder
Builder.load_file('GUI/kv_files/config_advanced_new.kv')

import os
import shutil
import sys
import re

if sys.version_info[0] == 3:
    import configparser
else:
    import ConfigParser as configparser


#from kivy.core.text import LabelBase
#LabelBase.register(name='Asana',
#fn_regular="data/fonts/Asana-Math.ttf")

class input(TextInput):
    pass

class LoadDialog(FloatLayout):
    '''
        Class responsible for add the window search files.
    '''
    load = ObjectProperty(None)
    cancel = ObjectProperty(None)
    id_input = ''

class ConfigAdvancedNew(Screen):
    '''
        Class responsible for the New Experiment screen's functions
        The methods dismiss_popup, show_load and load are responsible for
        to manage the window search files.
        The method open_help open the window when the question icon is clicked.
    '''


    def __init__(self,  **kwargs):
        super(ConfigAdvancedNew , self).__init__(**kwargs)


    def open_help(self, nomeArq):
        print(nomeArq)
        p = Popup_help(nomeArq)
        p.open()


    '''
        def quit(self):
            App.get_running_app().stop()
    '''
