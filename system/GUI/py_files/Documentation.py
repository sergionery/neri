# -*- coding: utf-8 -*-

from kivy.lang import Builder
from kivy.uix.floatlayout import FloatLayout

Builder.load_file('GUI/kv_files/documentation.kv')

class Documentation(FloatLayout):

    '''read text from "documentation.txt" and copy it in the label "help" from "documentation.kv" '''
    def Text_Label(self):
        filename = 'system/help/documentation.txt'
        fl = open(filename, 'rt')
        lst = fl.readlines()
        fl.close()

        text = ''

        for i in lst:
            text += i

        self.ids.doc.text = text

    def __init__(self, **kwargs):
        super(Documentation, self).__init__(**kwargs)
        self.Text_Label()
