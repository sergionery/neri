# -*- coding: utf-8 -*-

from kivy.uix.floatlayout import FloatLayout
from kivy.properties import ObjectProperty
from kivy.uix.screenmanager import Screen
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from GUI.py_files.Popup_help import *
from GUI.py_files.Hint import *
from GUI.py_files.filebrowser import FileBrowser
from lib.differential_analyses import *
from lib.differential_network import *
from GUI.py_files.ConfigAdvanced import *
from GUI.py_files.NewExperiment import *
from os.path import sep, expanduser, isdir, dirname, basename
from kivy.lang import Builder
import sys
import time
import re
import numpy as np
import pandas as pd

Builder.load_file('GUI/kv_files/analysis.kv')


class Analysis(Screen):

    '''
        Class responsible for manage the Analysis' screen functions.
        Class responsible for the New Experiment screen's functions
        The methods dismiss_popup, show_load and load are responsible for
        to manage the window search files.
        The method open_help open the window when the question icon is clicked.
    '''

    popup = Popup(title="Advanced Configuration",
    			  content= ConfigAdvanced(), size_hint=(.8, .8))

    ExpID = ''
    order_by = 'S'

    def __init__(self,  **kwargs):
        super(Analysis, self).__init__(**kwargs)


    def open_help(self, nomeArq):
        p = Popup_help(nomeArq)
        p.open()


    def dismiss_popup(self):
        self._popup.dismiss()

    def get_order_by(self, orderby):
        self.order_by = orderby    

    def ApplyFilter(self, df, top, order_by, x_ini=None, x_end=None, label='NODE'):
        ExpID = self.ExpID
        x_ini = float(df.min()['X'] if x_ini is None else x_ini)
        x_end = float(df.max()['X'] if x_end is None else x_end)
        #
        X = df['X']
        Y = df['Y']
        S = df['S']
        #
        Xrange = (X >= x_ini) & (X <= x_end)
        count  = Xrange.value_counts()[True]
        #
        top = count if top is None else top
        tagtime  = time.strftime('%Y%m%d_%H%M%S')
        name     = '%s_%s_Xini%s_TOP%s_4_Filtered.txt' % (ExpID, tagtime, str(x_ini), str(top))
        filename = 'user/output/%s/%s' % (ExpID, name)
        #
        df_Filtered = df[Xrange].sort_values([order_by], ascending=0)[:top]
        df_Filtered.to_csv(filename, sep="\t", index_label= label)


    def build(self, id_input):

        if sys.platform == 'win':
            user_path = dirname(expanduser('~')) + sep + 'Documents'
        else:
            user_path = expanduser('~') + sep + 'Documents'

        browser = FileBrowser(select_string='Select',
                              favorites=[(user_path, 'Documents')], id=id_input)

        browser.bind(
                    on_success = self._fbrowser_success,
                    on_canceled= self._fbrowser_canceled,
                    on_submit  = self._fbrowser_success) # carregar arquivo com dois cliques

        return browser

    def _fbrowser_canceled(self, instance):
        self.dismiss_popup()

    def _fbrowser_success(self, instance):
        self.ids[self._popup.content.id].text = "%s"%(instance.selection[0])
        self._popup.dismiss()

    def dismiss_popup(self):
        self._popup.dismiss()

    def differential_network(self):
        nodes_input = self.ids['nodes_input'].text
        edges_input = self.ids['edges_input'].text

        if(nodes_input != '' and edges_input != ''):
            args = [self.ExpID, nodes_input, edges_input]
            generate_network(args)
            content = Label(text = 'The file has been created')
            self.alert_popup(content)
        else:
            content = Label(text = 'Please, enter with nodes file \nand edges file')
            self.alert_popup(content)

    def generate(self):

        #TODO: Verificar como resgatar o valor do Xini e Xend do popup.
        #x_ini      = int(self.ids['xini_input'].text) #5
        x_ini      = 5
        x_end      = None
        percentile = int(self.popup.content.ids['percentile_input'].text) #75
        window     = int(self.popup.content.ids['window_input'].text)     #2
        slide      = int(self.popup.content.ids['slide_input'].text) #1
        func       = f_exp if self.popup.content.ids['func_input'].text == 'exponential' else f_powerlaw

        # Fazer o try exception para pegar a execeção.
        topNodes   = int(self.ids['topnodes_input'].text) #200
        topEdges   = int(self.ids['topedges_input'].text) #200
        
     

        #paramsNodes = (np.array([1.10788098, 0.42487181]), np.array([[0.02291229, 0.00559919],[0.00559919, 0.00149771]]))
        #paramsEdges = (np.array([1.10788098, 0.42487181]), np.array([[0.02291229, 0.00559919],[0.00559919, 0.00149771]]))
        #paramNodes = paramsNodes[0]
        #paramEdges = paramsEdges[0]
        ####paramNodes = WeightsToAllScores(weightsC, weightsD,...)[0]
        ####paramEdges = WeightsToAllScores(weightsC, weightsD,...)[0]

        fileNodes = self.ids['nodes_input'].text
        fileEdges = self.ids['edges_input'].text

        # Deprecated: Quantile genes or edges to be selected
        # only present for compatibility
        x_quant_Nodes = 0.00        
        x_quant_Edges = 0.00  

        if(fileNodes != '' and fileEdges != ''):

            #------------------------------------------------------------------------------
            # READS WEIGHTS FILE (RELATIVE IMPORTANCE)
            #------------------------------------------------------------------------------
            # NODES
            #
            dfNodesWeights = pd.read_table(fileNodes, index_col='NODE')
            #------------------------------------------------------------------------------
            # EDGES
            #
            dfEdgesWeights = pd.read_table(fileEdges, index_col=['NODE1','NODE2'])
            #------------------------------------------------------------------------------            

            #--------------------------------
            # NODES: DIFFERENTIAL ANALYSIS
            #--------------------------------
            dfScoresNodes, paramNodes, \
                x_ini_Nodes, topNodes = \
                    DifferentialAnalyses(dfNodesWeights, topNodes, x_quant_Nodes,
                                        func, window, slide, percentile, x_ini, x_end)

            #--------------------------------
            # EDGES: DIFFERENTIAL ANALYSIS
            #--------------------------------
            dfScoresEdges, paramEdges, \
                x_ini_Edges, topEdges = \
                    DifferentialAnalyses(dfEdgesWeights, topEdges, x_quant_Edges,
                                        func, window, slide, percentile, x_ini, x_end)

            #--------------------------------
            # WRITES DIFFERENTIAL ANALYSES TO FILE
            #--------------------------------

            prefixNodes = fileNodes.replace('3_NODES.txt', '') + \
                                            '4_NODES-xi%02.0f-top_%d' % (x_ini_Nodes*100, topNodes)

            prefixEdges = fileEdges.replace('3_EDGES.txt', '') + \
                                                '4_EDGES-xi%02.0f-top_%d' % (x_ini_Edges*100, topEdges)

            dfScoresNodes.reset_index(inplace=True)
            dfScoresNodes.to_csv(prefixNodes + '.txt', sep='\t', index=False)      

            dfScoresEdges.reset_index(inplace=True)
            dfScoresEdges.to_csv(prefixEdges + '.txt', sep='\t', index=False)
              
            #------------------------------------------------------------------------------
            #
            # PLOTTING (Matplotlib + Plotly)
            #
            #------------------------------------------------------------------------------

            #--------------------------------
            # NODES SELECTION (VOLCANO PLOT)
            #--------------------------------

            font, fig, func = SetPlotParams(language='en', function='power_law', 
                                            keyname='NODES', prefix = prefixNodes)


            VolcanoPlot_matplotlib(dfScoresNodes, topNodes, func, paramNodes, \
                        fig, font, x_ini_Nodes, x_end, plot_selection = True,\
                        Met_i=None, Met_label='', save_figure=True)


            VolcanoPlot_plotly(dfScoresNodes, topNodes, func, paramNodes, \
                        fig, font, x_ini_Nodes, x_end, plot_selection = True,\
                        Met_i=None, Met_label='', save_figure=True, keyname='NODE')                                                


            #--------------------------------
            # EDGES SELECTION (VOLCANO PLOT)
            #--------------------------------

            font, fig, func = SetPlotParams(language='en', function='power_law', 
                                            keyname='EDGES', prefix = prefixEdges)

            VolcanoPlot_matplotlib(dfScoresEdges, topEdges, func, paramEdges, \
                        fig, font, x_ini_Edges, x_end, plot_selection = True,\
                        Met_i=None, Met_label='', save_figure=True)

            dfScoresEdges['EDGE'] = dfScoresEdges['NODE1'] + ' <-> ' + dfScoresEdges['NODE2']
            VolcanoPlot_plotly(dfScoresEdges, topEdges, func, paramEdges, \
                        fig, font, x_ini_Edges, x_end, plot_selection = True,\
                        Met_i=None, Met_label='', save_figure=True, keyname='EDGE')


            # dfNodesS = pd.read_table(nodes_input,index_col='NODE',sep='\t')
            # #dfEdgesS = pd.read_table(edges_input,index_col='EDGE',sep='\t')
            # # Verificar porque a EDGE nao estah funcionando

            # DfScoresNodes, paramsNodes = WeightsToAllScores(dfNodesS['C'], dfNodesS['D'])
            # paramNodes = paramsNodes[0]

            # font, fig, func = defaultPlotParams(language='en', function=self.popup.content.ids['func_input'].text)

            # if(self.ExpID == ''):
            #     m = re.search("[A-Z_]+", basename(nodes_input))
            #     self.ExpID = m.group(0)[:-1] #verificar padrao

            # #self.ApplyFilter(DfScoresNodes, top, self.order_by, x_ini, x_end)


            # PlotFitting(DfScoresNodes, top, func, paramNodes, \
            #             fig, font, x_ini, x_end, plot_selection = True,\
            #             Met_i=None, Met_label='', save_figure=False)

                   
            # PlotFitting(dfEdgesS, top, func, paramEdges, \
            #            fig, font, x_ini, x_end, plot_selection = True,\
            #            Met_i=None, Met_label='', save_figure=False)
        else:
            content = Label(text = 'Please, enter with nodes file \nand edges file')
            self.alert_popup(content)

    def alert_popup(self, message):
        self._popup = Popup(title='', content=message,
                            #background_color = (0, 0, 0, 0)- deixar transparente,
                            size_hint=(.3, .3))
        self._popup.open()
        Clock.schedule_once(self._popup.dismiss, 3) #seconds

    def show_load(self, id_input, rootpath):
        self._popup = Popup(title=id_input, content=self.build(id_input),
                            size_hint=(.9, .9))
        self._popup.open()

    def show_config_advanced(self):
        self.popup.open()
        self.popup.content.ids['create_button'].bind(on_press=self.popup.dismiss)
