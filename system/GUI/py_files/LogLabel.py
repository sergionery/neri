# -*- coding: utf-8 -*-
from kivy.uix.label import Label
from kivy.lang import Builder

Builder.load_string("""
<LogLabel>:
    halign: 'left'
    text_size: self.size[0], None
    #text_size: self.size
    valign: "middle"
""")

class LogLabel(Label):
    pass
