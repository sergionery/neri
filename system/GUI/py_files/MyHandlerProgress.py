# -*- coding: utf-8 -*-

import logging
from kivy.clock import Clock

class MyHandlerProgress(logging.Handler):

    def __init__(self, progress_bar, level=logging.INFO):
        logging.Handler.__init__(self, level=level)
        self.progress_bar = progress_bar

    def emit(self, record):
        "using the Clock module for thread safety with kivy's main loop"
        def f(dt=None):
            self.progress_bar.value = float(self.format(record))*100
        Clock.schedule_once(f)
