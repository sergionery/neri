# -*- coding: utf-8 -*-

from kivy.uix.popup import Popup
from kivy.uix.image import Image
from kivy.lang import Builder

Builder.load_file('GUI/kv_files/popup_help.kv')

class Popup_help(Popup):

    '''read text from a txt file and copy it in the label "label_txt" from "popup_help.kv" '''
    def Text_Label(self, id_button):
        filename = 'system/GUI/txt_files/' + id_button
        arq = open(filename, 'rt')
        lst = arq.readlines()
        arq.close()

        text = ''

        for i in lst:
            text += i

        self.ids.label_txt.text = text

    def __init__(self, id_button):
        super(Popup_help, self).__init__()

        ''' "id_button" define the file name '''
        self.Text_Label(id_button)
