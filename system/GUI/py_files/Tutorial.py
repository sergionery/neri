# -*- coding: utf-8 -*-

from kivy.lang import Builder
from kivy.uix.floatlayout import FloatLayout

Builder.load_file('GUI/kv_files/tutorial.kv')

class Tutorial(FloatLayout):

    '''read text from "tutorial.txt" and copy it in the label "help" from "tutorial.kv" '''
    def Text_Label(self):
        filename = 'system/help/tutorial.txt'
        fl = open(filename, 'rt')
        lst = fl.readlines()
        fl.close()

        text = ''

        for i in lst:
            text += i

        self.ids.tutorial.text = text

    def __init__(self, **kwargs):
        super(Tutorial, self).__init__(**kwargs)
        self.Text_Label()
