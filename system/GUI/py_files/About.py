# -*- coding: utf-8 -*-

from kivy.uix.floatlayout import FloatLayout
from kivy.lang import Builder

Builder.load_file('GUI/kv_files/about.kv')

class About(FloatLayout):

    '''read text from a "about.txt" and copy it in the label "about" from "about.kv" '''
    def Text_Label(self):
        filename = 'system/help/about.txt'
        fl = open(filename, 'rt')
        lst = fl.readlines()
        fl.close()

        text = ''

        for i in lst:
            text += i
        self.ids.about.text = text

    def __init__(self, **kwargs):
        super(About, self).__init__(**kwargs)
        self.Text_Label()
