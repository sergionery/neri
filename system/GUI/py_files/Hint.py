# -*- coding: utf-8 -*-

from kivy.core.window import Window
from kivy.uix.label import Label
from kivy.clock import Clock
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.image import Image
from kivy.lang import Builder

Builder.load_file('GUI/kv_files/hint.kv')

class ImageButton(ButtonBehavior, Image):
	'''
		Class created for give the button's behavior  to image.
	'''
	pass

class Hint(Label):
	pass


class TooltipHint(ImageButton):
	'''
		Class responsible to create the help icon's hint.
		The method on_mouse_pos catch the mouse's position and verify if it is
		inside the icon, if there, the method display_tooltip is fired and show
		the text saved into the tooltip.
		When the mouse is not inside the icon, then the method close_tooltip is
		fired and the text disappear.
	'''

	hint = Hint()

	def __init__(self, *args, **kwargs):
		Window.bind(mouse_pos=self.on_mouse_pos)
		super(TooltipHint, self).__init__(**kwargs)

	def on_mouse_pos(self, *args):
		if not self.get_root_window():
			return
		pos = args[1]
		self.hint.pos = pos

		Clock.unschedule(self.display_tooltip) # cancel scheduled event since I moved the cursor
		self.close_tooltip() # close if it's opened

		if self.collide_point(*self.to_widget(*pos)):
			self.hint.text = self.text  # para mudar o texto do hint de acordo com o widget
			Clock.schedule_once(self.display_tooltip, 1)


	def close_tooltip(self, *args):
		Window.remove_widget(self.hint)

	def display_tooltip(self, *args):
		Window.add_widget(self.hint)
