# -*- coding: utf-8 -*-



__author__ = "Ka-Ping Yee <ping@lfw.org>"
__date__ = "26 February 2001"

__credits__ = """Guido van Rossum, for an excellent programming language.
Tommy Burnette, the original creator of manpy.
Paul Prescod, for all his work on onlinehelp.
Richard Chamberlain, for the first implementation of textdoc.
"""


from kivy.uix.floatlayout import FloatLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.properties import ObjectProperty
from kivy.uix.screenmanager import Screen
from kivy.clock import Clock
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.textinput import TextInput
from GUI.py_files.ConfigAdvancedNew import *
from GUI.py_files.Popup_help import *
from GUI.py_files.Hint import *
from GUI.py_files.filebrowser import FileBrowser
import os


from kivy.lang import Builder
Builder.load_file('GUI/kv_files/new_experiment.kv')
from os.path import *
import shutil
import sys
import re

if sys.version_info[0] == 3:
    import configparser
else:
    import ConfigParser as configparser


#from kivy.core.text import LabelBase
#LabelBase.register(name='Asana',
#fn_regular="data/fonts/Asana-Math.ttf")


class Input(TextInput):
    '''
        Class responsible to validate the text on 
        new experiment ID
    '''

class LoadDialog(FloatLayout):
    '''
        Class responsible for add the window search files.
    '''
    load = ObjectProperty(None)
    cancel = ObjectProperty(None)
    id_input = ''

class NewExperiment(Screen):
    '''
        Class responsible for the New Experiment screen's functions
        The methods dismiss_popup, show_load and load are responsible for
        to manage the window search files.
        The method open_help open the window when the question icon is clicked.

        Herança de `Screen`
    '''


    popup = Popup(title="Advanced Configuration",
    			  content= ConfigAdvancedNew(), size_hint=(.8, .8))

    ExpID = ''
    order_by = 'S'


    def __init__(self,  **kwargs):
        super(NewExperiment , self).__init__(**kwargs)

    def choosePPI(self):
        widgets = self.ids
        ppi_ids = [x for x in widgets.keys() if '__ppi_' in x]
        return [widgets[id].text for id in ppi_ids if widgets[id].active]

    def dismiss_popup(self):
        self._popup.dismiss()        

    def config_append(self, experiment):
        dir_seeds      = 'user/input/' + experiment + '/seeds'
        dir_expression = 'user/input/' + experiment + '/expression'

        os.makedirs(dir_seeds)
        os.makedirs(dir_expression)

        shutil.copy(self.ids['seeds_input'].text,      dir_seeds)
        shutil.copy(self.ids['expression_input'].text, dir_expression)

        config_file = open("user/input/config.ini", 'a')

        config = configparser.RawConfigParser()

        config.add_section(experiment)
        #keywords    = "'%s'" % str(self.ids['keywords_input'].text)
        description = "'%s'" % str(self.ids['description_input'].text)
        #col_seeds   = "%d" % int(self.ids['col_seeds_input'].text)
        col_seeds   = 1
        #epsilon     = self.ids['epsilon'].value
        epsilon = 0.05

        #config.set(experiment, 'keywords',    keywords)
        config.set(experiment, 'description', description)
        config.set(experiment, 'epsilon',     epsilon)
        config.set(experiment, 'col_seeds',   col_seeds)
        config.set(experiment, 'ppi', self.choosePPI())
        config.set(experiment, 'file_seeds', dir_seeds + "/"+ os.path.basename(self.ids['seeds_input'].text))
        config.set(experiment, 'file_expression', dir_expression + "/"+ os.path.basename(self.ids['expression_input'].text))

        config.write(config_file)
        config_file.close()

    def create(self):
        #TODO: Clean na tela de new depois que clicar em create

        experiment = self.ids['experiment_input'].text

        if self.ids['experiment_input'].text == '' or self.ids['expression_input'].text == '' \
        or self.ids['seeds_input'].text == '':
        
            content = Label(text = 'Please, fill entries before creating')
            self.alert_popup(content)            
        
        else:

            self.config_append(experiment)

            content = Label(text = 'The experiment '+experiment+ '\nhas been created')
            self.alert_popup(content)

    def alert_popup(self, message):
        self._popup = Popup(title='', content=message,
                            #background_color = (0, 0, 0, 0)- deixar transparente,
                            size_hint=(.3, .3))
        self._popup.open()
        Clock.schedule_once(self._popup.dismiss, 2) #seconds

    def open_help(self, nomeArq):
        p = Popup_help(nomeArq)
        p.open()

    def insert_text(self, substring, from_undo=False):
        #TODO: verificar o que eh o from_undo
        t = Input()
        pat = re.compile('[^a-zA-Z0-9_]+')
        self.text = substring

        if '.' in self.text:
            s = re.sub(pat, '', substring)
        else:
            s = '.'.join([re.sub(pat, '', s) for s in substring.split('.', 1)])

        self.ids['experiment_input'].text = s  #FIXME: ficou meio lerdinho
        return super(Input, t).insert_text(s, from_undo=from_undo)

    def build(self, id_input):

        if sys.platform == 'win':
            user_path = dirname(expanduser('~')) + sep + 'Documents'
        else:
            user_path = expanduser('~') + sep + 'Documents'

        browser = FileBrowser(select_string='Select',
                              favorites=[(user_path, 'Documents')], id=id_input)
        browser.bind(
                    on_success = self._fbrowser_success,
                    on_canceled= self._fbrowser_canceled,
                    on_submit  = self._fbrowser_success) # carregar arquivo com dois cliques

        return browser

    def _fbrowser_canceled(self, instance):
        self.dismiss_popup()

    def _fbrowser_success(self, instance):
        self.ids[self._popup.content.id].text = "%s"%(instance.selection[0])
        self._popup.dismiss()

    def dismiss_popup(self):
        self._popup.dismiss()

    def show_load(self, id_input, rootpath):
        popup_name = [id_input[:5] if id_input[:5] == "seeds" else id_input[:10]]
        popup_name = popup_name[0] + ' file'

        self._popup = Popup(title=popup_name, content=self.build(id_input),
                            size_hint=(.9, .9))
        self._popup.open()



    def show_config_advanced(self):
        self.popup.open()
        self.popup.content.ids['create_button'].bind(on_press=self.popup.dismiss)


    '''
        def quit(self):
            App.get_running_app().stop()
    '''
