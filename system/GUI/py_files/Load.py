# -*- coding: utf-8 -*-
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.screenmanager import Screen
from kivy.uix.dropdown import DropDown
from kivy.uix.button import Button
#from kivy.uix.listview import ListView
from kivy.properties import ObjectProperty
#from kivy.adapters.simplelistadapter import SimpleListAdapter
from kivy.lang import Builder
from kivy.clock import Clock
from GUI.py_files.Popup_help import *
from GUI.py_files.MyHandler import *
from GUI.py_files.LogLabel import *
from GUI.py_files.Popup_help import *
from GUI.py_files.Execute import Execute
import lib.data_integration as DI
import threading
import logging
import os
import sys
import time

if sys.version_info[0] == 3:
    import configparser
else:
    import ConfigParser as configparser

Builder.load_file('GUI/kv_files/load.kv')

import sys

if sys.version_info[0] == 3:
    import configparser
else:
    import ConfigParser as configparser

class myThread (threading.Thread):
    def __init__(self, threadID, retorno, log, args):
        super(myThread, self).__init__()
        self.threadID = threadID
        self.retorno = retorno
        self.log = log
        self.args = args

    def run(self):
        #self.retorno = logg_logging(self.log) #-> mudar nome da função
        #self.retorno = DI.DataIntegration(self.args[0],self.args[1],self.args[2],self.args[3], self.args[4], self.args[5], self.args[6])
        self.retorno = DI.DataIntegration(*self.args)


class Load(Screen):
    '''
        Class responsible for the New Experiment screen's functions
        The methods dismiss_popup, show_load and load are responsible for
        to manage the window search files.
        The method open_help open the window when the question icon is clicked.
    '''

    '''def dropdown(self):
        mylist = ['desease1', 'desease2', 'desease3', 'testestessss']
        self.dropdown = DropDown()

        for i in mylist:
            btn = Button(text=i, size_hint_y=None, height=44)
            btn.bind(on_release=lambda  btn: self.dropdown.select(btn.text))
            self.dropdown.add_widget(btn)

        self.ids.experiment_button.bind(on_release=self.dropdown.open)
        self.dropdown.bind(on_select=lambda instance, x: setattr(self.ids.experiment_input, 'text', x))
    '''


    def get_input_parameters(self, ExpID):
        config = configparser.RawConfigParser()
        config.read('user/input/config.ini')
        try:
            Sppi            =       config.get(ExpID, 'ppi')
            epsilon         = float(config.get(ExpID, 'epsilon'))
            file_expression =       config.get(ExpID, 'file_expression')
            col_seeds       =   int(config.get(ExpID, 'col_seeds'))
            file_seeds      =       config.get(ExpID, 'file_seeds')
            #
        except configparser.NoSectionError as err:
            print(err)
            print("Leaving application..")
            sys.exit()
        #
        Lppi = [ppi.strip("' ") for ppi in Sppi.strip('[]').split(',')]
        #
        return ExpID, epsilon, Lppi, file_expression, file_seeds, col_seeds


    def set_log_level(self, text):
        print("definindo nivel")
        if (text == 'debug'):
            self.log.setLevel(logging.DEBUG)
        elif (text == 'info'):
            self.log.setLevel(logging.INFO)
        else:
            self.log.setLevel(logging.NOTSET)



    def create_log(self):
        list_view = self.ids.scroll_desc

        #list_adapter = SimpleListAdapter(data=[],cls=LogLabel)
        #list_view.adapter = list_adapter
    

        #list_view.data = [{'text': str(LogLabel)}]

        self.log.addHandler(MyHandler(list_view))
        
        
        #SAIDA PARA O ARQUIVO
        #handler_file = logging.FileHandler("TESTE_LOG_INTERFACE.txt")
        #self.log.addHandler(handler_file)

        #-------------------------------

        ExpID = self.ids.experiment_input.text
        ExpID, epsilon, Lppi, file_expression, file_seeds, col_seeds = self.get_input_parameters(ExpID)

        args = (ExpID, epsilon, Lppi, file_expression, file_seeds, col_seeds, self.log)


        
        thread = myThread(1,(), self.log, args)
        tagtime  = time.strftime('%Y%m%d_%H%M%S')
        thread.start()
        thread.join()
        
        G,Seeds,SuperSeeds,C,D = thread.retorno
        Execute.args = (float(epsilon),G,Seeds,SuperSeeds,C,D)
        Execute.ExpID = ExpID
        Execute.tagtime = tagtime
        Execute.epsilon = epsilon

        content = Label(text = 'The experiment has been loaded')
        self.alert_popup(content)  

        self.ids.next_button_load.disabled = False



    def alert_popup(self, message):
        self._popup = Popup(title='', content=message,
                            #background_color = (0, 0, 0, 0)- deixar transparente,
                            size_hint=(.3, .3))
        self._popup.open()
        Clock.schedule_once(self._popup.dismiss, 2) #seconds


    def create_thread(self):
        t = threading.Thread( target=self.create_log )
        t.start()

    def create_dropdown(self):
        config = configparser.SafeConfigParser()
        directory = os.path.dirname("user/input/config.ini")
        if (os.path.exists(directory)):
            config.read("user/input/config.ini")
            mylist = config.sections()
            self.dropdown = DropDown(max_height=130)
            self.dropdown.container.spacing = 0

            for i in mylist:
                btn = Button(text=i, size_hint_y=None, height=30, background_color=(.9,.9,.9,1), background_normal='',
                color=(0,0,0,1),halign='left', valign='middle')
                btn.bind(size=btn.setter('text_size'))
            #text_size: self.size
                btn.bind(on_release=lambda  btn: self.dropdown.select(btn.text))
                self.dropdown.add_widget(btn)

            self.dropdown.open(self.ids.experiment_button)
            self.dropdown.bind(on_select=lambda instance, x: setattr(self.ids.experiment_input, 'text', x))


    def open_help(self, nomeArq):
        p = Popup_help(nomeArq)
        p.open()


    def __init__(self, **kwargs):
        self.log = logging.getLogger("my.logger")
        self.log.setLevel(logging.INFO)
        super(Load, self).__init__(**kwargs)