# -*- coding: utf-8 -*-
from kivy.uix.floatlayout import FloatLayout
from kivy.clock import Clock
import datetime
from kivy.properties import NumericProperty
from GUI.py_files.Popup_help import *
from GUI.py_files.Hint import *
from kivy.lang import Builder
from GUI.py_files.MyHandler import *
from GUI.py_files.MyHandlerProgress import *
#from GUI.py_files.LogLabel import *
import lib.path_selection as PS
from lib.differential_analyses import *
#from kivy.adapters.simplelistadapter import SimpleListAdapter
from GUI.py_files.Load import *
import threading
import logging
import time
import os

Builder.load_file('GUI/kv_files/execute.kv')


class myThread (threading.Thread):
    def __init__(self, threadID, result, args, log, progress_bar):
        super(myThread, self).__init__()
        self.threadID = threadID
        self.result = result
        self.args = args
        self.log = log
        self.progress = progress_bar

    def run(self):
        self.result = PS.ProcessingPathSelection(float(self.args[0]),self.args[1],self.args[2], self.args[3], self.args[4], self.args[5], self.log, self.progress)


class Execute(FloatLayout):

    ''' instance "Popup_help" passing the file name and call the open popoup function'''

    args = ()
    ExpID = ''
    tagtime = ''
    epsilon = 0
    number = NumericProperty()
    tempo = datetime.timedelta(seconds = 0)

    def __init__(self,  **kwargs):

        self.log = logging.getLogger("my.execute")
        self.log.setLevel(logging.INFO)

        #OUTPUT TO FILE
        #handler_file = logging.FileHandler("TESTE_LOG_INTERFACE-EXECUTE.txt")
        #self.log.addHandler(handler_file)

        self.progress_log = logging.getLogger("progress_bar")
        self.progress_log.setLevel(logging.INFO)

        super(Execute, self).__init__(**kwargs)
        self.help = TooltipHint(self.ids['help'].text)

        

    def open_help(self, filename):
        p = Popup_help(filename)
        p.open()

    def set_log_level(self, text):
        print("Setting LOG level")
        if (text == 'debug'):
            self.log.setLevel(logging.DEBUG)
        elif (text == 'info'):
            self.log.setLevel(logging.INFO)
        else:
            self.log.setLevel(logging.NOTSET)


    def create_log(self):
        self.start()

        list_view = self.ids.scroll_desc
        progress_bar = self.ids.progress_bar

        #list_adapter = SimpleListAdapter(data=[],cls=LogLabel)
        #list_view.adapter = list_adapter

        #list_view.data = [{"text": str(LogLabel)}]

        self.log.addHandler(MyHandler(list_view))
        self.progress_log.addHandler(MyHandlerProgress(progress_bar))


        print(self.args)

        #------------------------------------------------------------------------------

        path_exp = 'user/output/' + self.ExpID
        prefix   = '%s/%s_%s_E%02.0f-' % (path_exp, self.ExpID, self.tagtime, self.epsilon*100)

        if not os.path.exists(path_exp):
            os.makedirs(path_exp)

        #------------------------------------------------------------------------------

        print("STARTING EXECUTION: PathSelection")
        thread = myThread(1,10,self.args,self.log, self.progress_log)
        thread.start()
        thread.join()
        print("ENDING EXECUTION: PathSelection")
        print(thread.result)
        TupleC,TupleD = thread.result

        #------------------------------------------------------------------------------

        GraphC, infoC = TupleC
        GraphD, infoD = TupleD

        #------------------------------------------------------------------------------

        PS.StatsMatrix(infoC,self.log)
        PS.StatsMatrix(infoD,self.log)

        #------------------------------------------------------------------------------
        # RELATIVE IMPORTANCE OUTPUT
        #------------------------------------------------------------------------------

        # Write Relative Importance
        fileNodes, fileEdges = WriteScores(GraphC, GraphD, prefix, weight='weight')

        #------------------------------------------------------------------------------

        self.stop()

    
    
    def create_thread(self):
        t = threading.Thread(target=self.create_log )
        t.start()

    def increment_time(self, interval):
        self.tempo = self.tempo + datetime.timedelta(seconds = 1)
        self.ids.time.text = '%s'%(str(self.tempo))
    
    def start(self):
        self.tempo = datetime.timedelta(seconds = 0)
        Clock.unschedule(self.increment_time)
        #TODO: Test with .5 e 1
        Clock.schedule_interval(self.increment_time, 1)

    def stop(self):
        Clock.unschedule(self.increment_time)
