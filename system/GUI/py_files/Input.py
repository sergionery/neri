# -*- coding: utf-8 -*-

from kivy.uix.floatlayout import FloatLayout
from kivy.uix.popup import Popup
from GUI.py_files.Load import *
from GUI.py_files.NewExperiment import *
from kivy.lang import Builder
Builder.load_file('GUI/kv_files/input.kv')

class Input(FloatLayout):

    '''
        Main screen.
    '''

    def load_experiment(self):
    	content = Load()
    	self._popup = Popup(title="Load Experiment", content=content,
                            size_hint=(0.9, 0.9))
    	self._popup.open()

    def create_new(self):

    	content = NewExperiment()
    	self._popup = Popup(title="Create New Experiment", content=content,
                            size_hint=(.9, .9))
    	self._popup.open()



    def __init__(self, **kwargs):
        super(Input, self).__init__(**kwargs)
