# -*- coding: utf-8 -*-

from kivy.uix.floatlayout import FloatLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.scrollview import ScrollView
from kivy.lang import Builder

Builder.load_file('GUI/kv_files/contactus.kv')

class Contact_us(ScrollView):
    ''' read the name of the developers from "contacts.txt"
    and instance class "Contact" for each contact passing the image and contact information path '''
    def Text_Label(self):
        filename = 'system/help/contatos.txt'
        fl = open(filename, 'rt')
        contacts = fl.readlines()
        fl.close()

        for contact in contacts:
            filename = 'system/help/' + contact.strip() + '.txt'
            imagename = 'GUI/images/' + contact.strip() + '.jpg'
            fl = open(filename, 'rt')
            inf = fl.readlines()
            information = ''
            for i in inf:
                information+= i;
            self.ids.box.add_widget(Contact(text=information, image=imagename))

    def __init__(self, **kwargs):
        super(Contact_us, self).__init__(**kwargs)
        self.Text_Label()


class Contact(BoxLayout):
    def __init__(self, text='', image='', **kwargs):
        super(Contact, self).__init__(**kwargs)
        self.ids.label.text = text
        self.ids.image.source = image
