# -*- coding: utf-8 -*-

from kivy.uix.floatlayout import FloatLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.properties import ObjectProperty
from kivy.uix.screenmanager import Screen
from kivy.uix.button import Button
from kivy.uix.dropdown import DropDown
from kivy.clock import Clock
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.textinput import TextInput
from GUI.py_files.Popup_help import *
from GUI.py_files.Hint import *
from GUI.py_files.filebrowser import FileBrowser
from os.path import sep, expanduser, isdir, dirname


from kivy.lang import Builder
Builder.load_file('GUI/kv_files/config_advanced.kv')

import os
import shutil
import sys
import re

if sys.version_info[0] == 3:
    import configparser
else:
    import ConfigParser as configparser


#from kivy.core.text import LabelBase
#LabelBase.register(name='Asana',
#fn_regular="data/fonts/Asana-Math.ttf")

class input(TextInput):
    pass

class LoadDialog(FloatLayout):
    '''
        Class responsible for add the window search files.
    '''
    load = ObjectProperty(None)
    cancel = ObjectProperty(None)
    id_input = ''

class ConfigAdvanced(Screen):
    '''
        Class responsible for the New Experiment screen's functions
        The methods dismiss_popup, show_load and load are responsible for
        to manage the window search files.
        The method open_help open the window when the question icon is clicked.
    '''


    def __init__(self,  **kwargs):
        super(ConfigAdvanced , self).__init__(**kwargs)



    def open_help(self, nomeArq):
        print(nomeArq)
        p = Popup_help(nomeArq)
        p.open()


    def create(self):
        #TODO: Clean na tela de new depois que clicar em create

        disease = self.ids['experiment_input'].text
        self.config_append(disease)

        content = Label(text = 'Created Successfully')
        self.alert_popup(content)


    def create_dropdown(self):
        config = configparser.SafeConfigParser()
        #directory = os.path.dirname("user/input/config.ini")
        #if (os.path.exists(directory)):
        config.read("user/input/config.ini")
        mylist = ['power_law', 'exponential']
        self.dropdown = DropDown(max_height=130)
        self.dropdown.container.spacing = 0

        for i in mylist:
            btn = Button(text=i, size_hint_y=None, height=30, background_color=(.9,.9,.9,1), background_normal='',
            color=(0,0,0,1),halign='left', valign='middle')
            btn.bind(size=btn.setter('text_size'))
        #text_size: self.size
            btn.bind(on_release=lambda  btn: self.dropdown.select(btn.text))
            self.dropdown.add_widget(btn)

        self.dropdown.open(self.ids['experiment_button'])
        self.dropdown.bind(on_select=lambda instance, x: setattr(self.ids['func_input'], 'text', x))


    def insert_text(self, substring, from_undo=False):
        #TODO: verificar o que eh o from_undo
        t = input()
        pat = re.compile('[^a-zA-Z0-9_]+')
        self.text = substring

        if '.' in self.text:
            s = re.sub(pat, '', substring)
        else:
            s = '.'.join([re.sub(pat, '', s) for s in substring.split('.', 1)])

        self.ids['experiment_input'].text = s  #FIXME: ficou meio lerdinho
        return super(input, t).insert_text(s, from_undo=from_undo)

    '''
        def quit(self):
            App.get_running_app().stop()
    '''
