# -*- coding: utf-8 -*-

import logging
from kivy.clock import Clock

class MyHandler(logging.Handler):
    def __init__(self, list_view, level=logging.INFO):
        logging.Handler.__init__(self, level=level)
        self.list_view = list_view
        self.l = []

    def emit(self, record):
        "using the Clock module for thread safety with kivy's main loop"
        self.l.append(record)
        def f(dt=None):
            self.list_view.data = [{"text": self.format(x)} for x in self.l]
        Clock.schedule_once(f)