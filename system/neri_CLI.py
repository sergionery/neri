# -*- coding: utf-8 -*-
#    Copyright (C) 2015-2018 by
#    Sérgio Nery Simões <sergionery@gmail.com>
#    All rights reserved.
#    BSD license.
#
# Author: Sérgio Nery Simões <sergionery@gmail.com>

#-----------------------------------------------------------------------------
"""
Executes the NERI script by using Command Line Interface.

Syntax:
    python3 neri.py <ExpID>
"""
#-----------------------------------------------------------------------------

from system.lib.data_integration import *
from system.lib.path_selection import *
from system.lib.differential_analyses import *

import logging
import sys
import time
import os

if sys.version_info[0] == 3:
    import configparser
else:
    import ConfigParser as configparser

def get_input_parameters(ExpID):
    config = configparser.RawConfigParser()
    config.read('user/input/config.ini')
    try:
        Sppi            =       config.get(ExpID, 'ppi')
        epsilon         = float(config.get(ExpID, 'epsilon'))
        file_expression =       config.get(ExpID, 'file_expression')
        col_seeds       =   int(config.get(ExpID, 'col_seeds'))
        file_seeds      =       config.get(ExpID, 'file_seeds')
        #
    except configparser.NoSectionError as err:
        print(err)
        print("Leaving application..")
        sys.exit()
    #
    Lppi = [ppi.strip("' ") for ppi in Sppi.strip('[]').split(',')]
    #
    return Lppi, epsilon, file_expression, file_seeds, col_seeds

#-----------------------------------------------------------------------------
#                           MAIN FUNCTION
#-----------------------------------------------------------------------------
def main(ExpID):
    #-----------------------------------------------------------------------------
    #                           DATA INTEGRATION
    #-----------------------------------------------------------------------------
    tagtime  = time.strftime('%Y%m%d_%H%M%S')
    Lppi, epsilon, file_expression, \
            file_seeds, col_seeds = get_input_parameters(ExpID)

    path_exp = 'user/output/' + ExpID
    prefix   = '%s/%s_%s_E%02.0f-' % (path_exp, ExpID, tagtime, epsilon*100)
    file_log = prefix + '1_LOG.txt'

    if not os.path.exists(path_exp):
        os.makedirs(path_exp)

    logger       = logging.getLogger()
    handler      = logging.StreamHandler()
    handler_file = logging.FileHandler(file_log)
    logger.addHandler(handler)
    logger.addHandler(handler_file)
    logger.setLevel(logging.INFO)

    progress_log = logging.getLogger()
    progress_log.addHandler(handler)
    progress_log.setLevel(logging.INFO)

    DI = DataIntegration(ExpID, epsilon, Lppi, file_expression, file_seeds, col_seeds, logger)
    G,Seeds,SuperSeeds,C,D = DI

    #-----------------------------------------------------------------------------
    #                       SHORTEST PATH SELECTION
    #-----------------------------------------------------------------------------

    TupleC,TupleD = ProcessingPathSelection(epsilon,G,Seeds,SuperSeeds,C,D,logger,progress_log)

    GraphC, infoC = TupleC
    GraphD, infoD = TupleD

    #-----------------------------------------------------------------------------
    # SUMMARY
    ###TODO: CREATE NEW FILE PREFIX + 2_SUMMARY.txt #
    #-----------------------------------------------------------------------------
    StatsMatrix(infoC,logger)
    StatsMatrix(infoD,logger)

    #------------------------------------------------------------------------------
    # RELATIVE IMPORTANCE OUTPUT
    #------------------------------------------------------------------------------
    # Write Relative Importance
    fileNodes, fileEdges = WriteScores(GraphC, GraphD, prefix, weight='weight')


    # Top N genes or edges to be selected
    topNodes = 100
    topEdges = 200
    # Deprecated: Quantile genes or edges to be selected
    # only present for compatibility
    x_quant_Nodes = 0.00
    x_quant_Edges = 0.00

    #------------------------------------------------------------------------------
    # Parameters (Sliding Window)
    #------------------------------------------------------------------------------
    func       = f_powerlaw
    window     = 2
    slide      = 1
    percentile = 75
    x_ini      = None      # used only x_quant == NAN
    x_end      = None   # Not used

    #-----------------------------------------------------------------------------
    #
    # # In case the output of path selection was saved into a file
    # #GraphC = NodesEdges2Graph(NodesC, EdgesC)
    # #GraphD = NodesEdges2Graph(NodesD, EdgesD)
    #
    #-----------------------------------------------------------------------------

    #------------------------------------------------------------------------------
    #
    # DIFFERENTIAL NETWORK ANALYSES
    #
    #------------------------------------------------------------------------------

    #------------------------------------------------------------------------------
    # READS WEIGHTS FILE (RELATIVE IMPORTANCE)
    #------------------------------------------------------------------------------
    # NODES
    #
    dfNodesWeights = pd.read_table(fileNodes, index_col='NODE')
    #------------------------------------------------------------------------------
    # EDGES
    #
    dfEdgesWeights = pd.read_table(fileEdges, index_col=['NODE1','NODE2'])
    #------------------------------------------------------------------------------

    #--------------------------------
    # NODES: DIFFERENTIAL ANALYSIS
    #--------------------------------
    dfScoresNodes, paramNodes, \
        x_ini_Nodes, topNodes = \
            DifferentialAnalyses(dfNodesWeights, topNodes, x_quant_Nodes,
                                 func, window, slide, percentile, x_ini, x_end)
    #--------------------------------
    # EDGES: DIFFERENTIAL ANALYSIS
    #--------------------------------
    dfScoresEdges, paramEdges, \
        x_ini_Edges, topEdges = \
            DifferentialAnalyses(dfEdgesWeights, topEdges, x_quant_Edges,
                                 func, window, slide, percentile, x_ini, x_end)

    #--------------------------------
    # WRITES DIFFERENTIAL ANALYSES TO FILE
    #--------------------------------

    prefixNodes = fileNodes.replace('3_NODES.txt', '') + \
                                    '4_NODES-xi%02.0f-top_%d' % (x_ini_Nodes*100, topNodes)

    prefixEdges = fileEdges.replace('3_EDGES.txt', '') + \
                                    '4_EDGES-xi%02.0f-top_%d' % (x_ini_Edges*100, topEdges)

    dfScoresNodes.reset_index(inplace=True)
    dfScoresNodes.to_csv(prefixNodes + '.txt', sep='\t', index=False)

    dfScoresEdges.reset_index(inplace=True)
    dfScoresEdges.to_csv(prefixEdges + '.txt', sep='\t', index=False)

    #------------------------------------------------------------------------------
    #
    # PLOTTING (Matplotlib + Plotly)
    #
    #------------------------------------------------------------------------------

    #--------------------------------
    # NODES SELECTION (VOLCANO PLOT)
    #--------------------------------

    font, fig, func = SetPlotParams(language='en', function='power_law', 
                                    keyname='NODES', prefix = prefixNodes)

    VolcanoPlot_matplotlib(dfScoresNodes, topNodes, func, paramNodes, \
                fig, font, x_ini_Nodes, x_end, plot_selection = True,\
                Met_i=None, Met_label='', save_figure=True)

    VolcanoPlot_plotly(dfScoresNodes, topNodes, func, paramNodes, \
                fig, font, x_ini_Nodes, x_end, plot_selection = True,\
                Met_i=None, Met_label='', save_figure=True, keyname='NODE')

    #--------------------------------
    # EDGES SELECTION (VOLCANO PLOT)
    #--------------------------------

    font, fig, func = SetPlotParams(language='en', function='power_law', 
                                    keyname='EDGES', prefix = prefixEdges)

    VolcanoPlot_matplotlib(dfScoresEdges, topEdges, func, paramEdges, \
                fig, font, x_ini_Edges, x_end, plot_selection = True,\
                Met_i=None, Met_label='', save_figure=True)

    dfScoresEdges['EDGE'] = dfScoresEdges['NODE1'] + ' <-> ' + dfScoresEdges['NODE2']
    VolcanoPlot_plotly(dfScoresEdges, topEdges, func, paramEdges, \
                fig, font, x_ini_Edges, x_end, plot_selection = True,\
                Met_i=None, Met_label='', save_figure=True, keyname='EDGE')

#-----------------------------------------------------------------------------
#                       MAIN
#-----------------------------------------------------------------------------

if __name__ == '__main__':
    main(sys.argv)
