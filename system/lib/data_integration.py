# -*- coding: utf-8 -*-
#    Copyright (C) 2015-2018 by
#    Sérgio Nery Simões <sergionery@gmail.com>
#    All rights reserved.
#    BSD license.
#
# Author: Sérgio Nery Simões <sergionery@gmail.com>

#-----------------------------------------------------------------------------

"""
Integrate PPI datasets and gene expression databases.

Read files of seeds, PPIs, gene expression data
    in two conditions: (C)ontrol and (D)isease.

Construct weighted PPI network graph.
"""

#-----------------------------------------------------------------------------

import networkx as nx
import pandas as pd
import re

#-----------------------------------------------------------------------------

def ReadGeneMapping(filename, log):
    '''
    Reads gene information File.

    Parameters
    ----------
        filename: gene's information file
            first line  := header with field names
            other lines := genes names/synonimous/id's, etc.

    Returns
    -------
        Dictionaries:
             D_geneid: Dictionary of geneid -> gene symbol
             D_symbol: Dictionary of gene symbols -> geneid
             D_xrefs:  Dictionary of dbXrefs -> geneid
    '''
    f = open(filename, 'r')
    # reads the first line (header)
    line = f.readline()
    #
    D_geneid  = {}   # Dictionary of geneid -> gene symbol
    D_symbol  = {}   # Dictionary of gene symbols -> geneid
    D_synonym = {}   # Dictionary of synonyms (gene symbols) -> geneid
    D_xrefs   = {}   # Dictionary of dbXrefs -> geneid
    #
    for line in f:
        lst = line.split('\t')
        # reads fields
        geneid    = int(lst[1])
        symbol    = lst[2]
        synonyms  = lst[4].split('|')
        dbxrefs   = lst[5].split('|')
        genetype  = lst[6]
        authority = lst[7]
        #
        # Gene Symbol <==> GeneID
        if (symbol != '-' and geneid != '-'):
            if D_geneid.get(geneid):
                log.debug('%d => %s | %s => %d' % (geneid, D_geneid[geneid], symbol, geneid))
            if D_symbol.get(symbol):
                log.debug('%d => %s | %s => %d' % (geneid, D_symbol[symbol], symbol, geneid))
            #
            D_geneid[geneid]         = [symbol]
            D_symbol[symbol.upper()] = geneid
            #
            # Synonyms|Symbol <==> GeneID
            if synonyms != ['-']:
                for s in synonyms:
                    D_geneid [geneid]    += [s]
                    D_symbol [s.upper()]  = geneid
                    D_synonym[s.upper()]  = geneid
        #
        #########################################################################
        #FIXME: como mostrar a lista de forma organizada ?
        #FIXME: sera que ao inves de redirecionar para a tela, podemos colocar
        # num arquivo ? estah travando muito, dificuldade em renderizar.
        #########################################################################
        if log.level == 10: #'DEBUG'
            pass
            #log.info('Not found: %s'% lst[:3])
            #log.info('\n')
        #
        # dbXrefs --> GeneID
        if dbxrefs != ['-']:
            for d in dbxrefs:
                #dbx,idx = d.split(':')
                dbx,idx = d.rsplit(':',1) # <== new version of Homo_sapiens.gene_info
                D_xrefs[dbx] = D_xrefs.get(dbx,{})
                D_xrefs[dbx][idx] = geneid
        #
    f.close()
    return D_geneid, D_symbol, D_xrefs, D_synonym

#-----------------------------------------------------------------------------
def SummariseGeneMapping(D_geneid, D_symbol, D_xrefs, D_synonym, log):
    log.info('GeneID:  %6d' % len(D_geneid))
    log.info('Symbol:  %6d' % len(D_symbol))
    log.info('Synonym: %6d' % len(D_synonym))
    log.info('dbXrefs: %6d' % len(D_xrefs))
    for k in D_xrefs.keys():
        log.info('%14d%8s' % (len(D_xrefs[k]),k))

#-----------------------------------------------------------------------------

def ReadInteractome(filename, symbol, dbname, log):
    '''
    Creates Interactome Graph from interaction file.

    Parameters
    ----------
        filename: interaction file
        symbol:   dictionary of gene symbols
                    key:   gene symbol
                    value: gene ID
        dbname:   string
                    PPI database name: 'hprd'|'mint'|'intact'

    Returns
    -------
        G: Interactome's Graph
            only genes present in interactome's file
            and gene symbol are added.
    '''
    G = nx.Graph()
    f = open(filename, 'r')
    #--------------------------------
    def get_gene_biogrid(i,L):
        taxid ='9606' # human taxid
        cond  = (L[15] == taxid and L[16] == taxid)
        g = L[i] if (cond == True) else '-'
        return g
    #
    #--------------------------------
    def get_gene_hprd(i,L):
        return L[i]
    #
    def get_gene_intact(i,L):
        taxid ='taxid:9606(Human)'
        cond  = (L[9] == taxid and L[10] == taxid)
        expr  = re.search('(uniprotkb:)([A-Za-z0-9_-]+)(\(gene name\))', L[i])
        g = expr.group(2) if (expr is not None) and (cond == True) else '-'
        return g
    #
    def get_gene_mint(i,L):
        taxid = 'taxid:9606(Homo sapiens)'
        cond  = (L[9] == taxid and L[10] == taxid)
        g = L[i] if cond == True else '-'
        return g
        # to do: deal with multiples genes/proteins - complex(?)
        # G = L[i].split(';')
        #return G
    #--------------------------------
    dbname.lower()
    a,b,has_header,get_gene = {
        'biogrid':        (7, 8, True, get_gene_biogrid),
        'biogrid_geneid': (1, 2, True, get_gene_biogrid),
        # OBSOLETE (all gene symbols)
        'hprd':           (0, 3, False, get_gene_hprd),
        'intact':         (4, 5, True,  get_gene_intact),
        'mint':           (4, 5, True,  get_gene_mint)
        }[dbname]
    #
    start = 1 # start line (offset) index
    # if db has header, skips first line
    if has_header == True:
        line   = f.readline()
        start += 1
    #
    log.debug('<LINE>:%16s%16s' % ('<GENE_A>', '<GENE_B>'))
    for i,line in enumerate(f,start):
        lst = line.strip().split('\t')
        geneA = get_gene(a,lst).upper()
        geneB = get_gene(b,lst).upper()
        if (geneA in symbol) and (geneB in symbol):
            G.add_edge(geneA, geneB)
        elif geneA != '-' and geneB != '-':
            log.debug('%-8d:%16s%16s' % (i, geneA, geneB))
    f.close()
    return G

#-----------------------------------------------------------------------------

def CompareSets(title,sA,sB,A,B, log):
    '''
    Compare sets A and B by printing some statistics.

    Parameters
    ----------
        title:  (string) title of comparison
        A:      set A
        B:      set B
        sA:     (string) name of set A
        sB:     (string) name of set B
    '''

    log.info('%s:' % (title))
    log.info('%16s:%6d' % (sA,len(A)))
    log.info('%16s:%6d' % (sB,len(B)))
    #
    log.info('        (%s & %s):%6d' % (sA,sB,len(A & B)))
    log.info('        (%s | %s):%6d' % (sA,sB,len(A | B)))
    log.info('        (%s - %s):%6d' % (sA,sB,len(A - B)))
    log.info('        (%s - %s):%6d' % (sB,sA,len(B - A)))

#-----------------------------------------------------------------------------

def SummariseInteractome(dict_dbgraph, log):
    log.info('DBName  Number_of_Nodes  Number_of_Edges')
    #
    list_dbgraph = list(dict_dbgraph.items()) # -- py3 Style
    #
    for dbname,Graph in list_dbgraph:
        nnodes = Graph.number_of_nodes()
        nedges = Graph.number_of_edges()
        log.info('%s  %d  %d' % (dbname.upper(),nnodes,nedges))
    #
    log.info('')
    if len(list_dbgraph) > 1:
        # sort each edge before comparing
        # sort_edge = lambda (u,v): (u,v) if u <= v else (v,u)
        sort_edge = lambda u,v: (u,v) if u <= v else (v,u) # -- py3 Style
        #
        for i,(dbA,GA) in enumerate(list_dbgraph[:-1],1):
            for dbB,GB in list_dbgraph[i:]:
                # NODES
                nodesA = set(GA.nodes())
                nodesB = set(GB.nodes())
                # EDGES
                edgesA = set(sort_edge(e[0], e[1]) for e in GA.edges()) # -- py3 Style
                edgesB = set(sort_edge(e[0], e[1]) for e in GB.edges()) # -- py3 Style
                #
                dbA = dbA.upper()
                dbB = dbB.upper()
                log.info('%s(%d/%d) X %s(%d/%d)' %
                              (dbA,len(nodesA),len(edgesA),
                               dbB,len(nodesB),len(edgesB)))
                CompareSets('NODES',dbA,dbB,nodesA,nodesB, log)
                CompareSets('EDGES',dbA,dbB,edgesA,edgesB, log)
                log.info('')
        #
        # compare all interactomes
        db,G = list_dbgraph[0]
        inter_nodes = set(G.nodes())
        union_nodes = set(G.nodes())
        inter_edges = set(sort_edge(a,b) for (a,b) in G.edges())
        union_edges = set(sort_edge(a,b) for (a,b) in G.edges())
        for dbi,Gi in list_dbgraph[1:]:
            db += ' ' + dbi
            nodesI = set(Gi.nodes())
            edgesI = set(sort_edge(e[0], e[1]) for e in Gi.edges())
            inter_nodes &= nodesI
            union_nodes |= nodesI
            inter_edges &= edgesI
            union_edges |= edgesI
        #
        log.info('ALL INTERACTOMES (%s)' % db.upper())
        log.info('        INTERSECTION:')
        log.info('                NODES:%6d' % len(inter_nodes))
        log.info('                EDGES:%6d' % len(inter_edges))
        log.info('        UNION:')
        log.info('                NODES:%6d' % len(union_nodes))
        log.info('                EDGES:%6d' % len(union_edges))

#-----------------------------------------------------------------------------

def ComposeGraphs(Graphs):
    '''
    Constructs a new PPI's Graph by integrating all PPI.

    Parameters
    ----------
        Graphs: List of PPI's Graph.

    Returns
    -------
        G:  Graph
            Composing of all interactomes from list Graphs.
    '''
    G = Graphs[0].__class__()

    for H in Graphs:
        nodes = (n for n in iter(H.nodes())) # -- py3 Style
        G.add_nodes_from(nodes)
        edges = ((u,v) for u,v in iter(H.edges()))  # -- py3 Style
        G.add_edges_from(edges)

    return G

#-----------------------------------------------------------------------------

def IntegrateData(GraphPPI, dExp):
    '''
    Constructs a INTEGRATED PPI's Graph by
    integrating a PPI Graph with Expression data.

    Only genes present in dExp will be added
    to interactome G.

    Parameters
    ----------
        GraphPPI: PPI Graph
        dExp: Dictionary of expression data.

    Returns
    -------
        G:  Graph
            Integrated PPI whose node names are
            present in both PPI Graph and
            dictionary dExp.
    '''
    H = GraphPPI
    G = GraphPPI.__class__()
    #
    nodes = (n for n in iter(H.nodes()) if n in dExp) # -- py3 Style
    G.add_nodes_from(nodes)
    edges = ((u,v) for u,v in iter(H.edges()) if u in dExp and v in dExp) # -- py3 Style
    G.add_edges_from(edges)
    #
    return G

#-----------------------------------------------------------------------------

def ReadExpressionFile(filename):
    '''
    Reads Expression File of Transcripts

    Parameters
    ----------
        filename: file of transcripts expression
            first line  := <tab><tab><list of ints {1:control,2:disease}>
            other lines := <genesymbol><accession><list of floats (expression)>

    Returns
    -------
        C,D:      DataFrames of control, Disease
    '''
    df_raw = pd.read_table(filename)
    df_exp = pd.pivot_table(df_raw, index = ['GENE_SYMBOL'], dropna = True, aggfunc = 'median')
    #
    C = df_exp.filter(regex = 'C.*')
    D = df_exp.filter(regex = 'D.*')
    #
    return (C, D)

#-----------------------------------------------------------------------------


def ReadSeedsFile(filename, col_seeds, log):
    '''
    Reads Seeds file from column col_seeds.

    Parameters
    ----------
        filename: str, seeds file.
        col_seeds:    int,  field index of gene
        debug:  boolean, check if file has
                  genes with duplicated lines.
    Returns
    -------
        D: dictionary, containing seeds.
    '''
    f = open(filename, 'r')
    D = {}
    for line in f:
        lst = line.strip().split('\t')
        if len(lst) >= col_seeds+1:
            gene = lst[col_seeds].strip('" \n')
            D[gene] = D.get(gene,0) + 1
    f.close()
    #
    if log.level == 10: #'DEBUG'
        for gene in D.keys():
            if D[gene] > 1:
                log.debug(gene, D[gene])
    #
    return D

#-----------------------------------------------------------------------------

def IncreaseSeeds(Seeds, G):
    '''
    Expands the Seeds set by adding its neighbors to it.
    '''
    DSS = {}
    for s in Seeds:
        DSS[s] = {}
        for n in G.neighbors(s):
            DSS[n] = {}
    #
    SuperSeeds = sorted(list(DSS))
    return SuperSeeds

#-----------------------------------------------------------------------------

def DataIntegration(ExpID, epsilon, Lppi,
                    file_expression,
                    file_seeds, col_seeds,
                    log):
    log.info('Reading MAPPING')
    #----------------------------------------
    # system files
    ########################## APENAS TESTE. VOLTAR OLD PARA NEW
    #file_geneinfo = 'system/mapping/Homo_sapiens.gene_info'
    file_geneinfo = 'system/mapping/Homo_sapiens.gene_info-OLD0'
    #############################################################
    MGeneid,MSymbol,MXrefs,MSynonym = ReadGeneMapping(file_geneinfo, log)
    SummariseGeneMapping(MGeneid,MSymbol,MXrefs,MSynonym, log)

    Dppi = {'biogrid_symbol': 'system/ppi/biogrid/BIOGRID-ALL-3.4.157.tab2.txt',
            'biogrid': 'system/ppi/biogrid/BIOGRID-ALL-3.4.157.tab2.txt',
            'hprd':    'system/ppi/hprd/BINARY_PROTEIN_PROTEIN_INTERACTIONS.txt',
            'mint':    'system/ppi/mint/2012-08-01-mint-human.txt',
            'intact':  'system/ppi/intact/intact.txt' }
    #----------------------------------------
    # constructs PPI
    file = {}
    Gppi = {}
    for ppi in Lppi:
        #----------------------------------------
        # <<<INFO LEVEL>>>
        log.info("Reading PPI: %s" % ppi)
        #----------------------------------------
        file[ppi] = Dppi[ppi]
        Gppi[ppi] = ReadInteractome(file[ppi], MSymbol, ppi, log)
    #
    SummariseInteractome(Gppi, log)
    #----------------------------------------
    # Integrates All PPI's Graphs with Expression Data

    C, D = ReadExpressionFile(file_expression)

    Graphs = list(Gppi.values())
    CG = ComposeGraphs(Graphs)

    IG = IntegrateData(CG, C.index)

    #G = list(nx.connected_component_subgraphs(IG))[0]
    #G = list(max(nx.connected_components(IG), key=len))   
    #G = next(IG.subgraph(c) for c in nx.connected_components(IG))
    #G = next(nx.connected_component_subgraphs(IG))
    #G = max(nx.connected_components(IG), key=len)

    #G =  next(( IG.subgraph(c).copy() for c in max(nx.connected_components(IG), key=len) ))

    #FIXME: verificar como otimizar isso -- 
    H = [IG.subgraph(c).copy() for c in sorted(nx.connected_components(IG), key=len, reverse=True)]
    G = H[0]

    #print(G.nodes())
    #G.remove_edges_from(G.selfloop_edges())


    dictSeeds = ReadSeedsFile(file_seeds, col_seeds, log)
    #----------------------------------------
    # reads seeds file
    Seeds = sorted(set(G) & set(dictSeeds))

    SuperSeeds     = IncreaseSeeds(Seeds, G)

    return (G,Seeds,SuperSeeds,C,D)
