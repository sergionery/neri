# -*- coding: utf-8 -*-
#    Copyright (C) 2015-2018 by
#    Sérgio Nery Simões <sergionery@gmail.com>
#    All rights reserved.
#    BSD license.
#
# Author: Sérgio Nery Simões <sergionery@gmail.com>
 
"""
Select best paths from seeds to neighbours on the weighted PPI network graph,
and compute relative importance of each gene and link.
"""
 
#-----------------------------------------------------------------------------
 
from __future__ import division, generators#, print_function
import networkx as nx
import numpy as np
from scipy.stats.stats import spearmanr
import time
import bisect

from multiprocessing import Pool

#-----------------------------------------------------------------------------
 
def ComputeAverageSpearmanMatrix(M):
    '''
    Computes Average Rho from Upper Triangular Spearman Matrix.
 
    Parameters:
    -----------
        M: upper triangular matrix of Spearman Correlations.
 
    Returns:
    --------
        rho_av: the average of Spearman correlations of the
                upper triangular matrix M.
    '''
    m = M.shape[1]
    #
    total  = 0
    rho_av = 0.0
    for i in range(m-1):
        for j in range(i+1, m):
            rho_av += M[i,j]
            total += 1
    #
    rho_av /= total
    return rho_av
 
#-----------------------------------------------------------------------------
 
def ReverseRankSpearmanMatrix(M, k):
    '''
    Reverses all the Spearman Correlations related to variable of order k.
    Stores the Spearman Correlation, of all combinations of rankings 2 by 2
    in an upper triangular matrix M.
 
    It has similar effect to reverse the ranks order of variable k.
    To reverse a ranking of a given variable, it is necessary replace the
    greatest rank by the smallest, the 2nd greatest by the 2nd smallest
    and so on.
 
    Parameters:
    -----------
        M: upper triangular matrix of Spearman Correlations.
        k: variable order to reverse on matrix M.
 
    Returns:
    --------
        M as a modified matrix.
    '''
    # Reverses column k from line 0 until line (k-1)
    for i in range(k):
        M[i,k] *= -1
    #
    # Reverses line k from column (k+1) until last column (m-1)
    n, m = np.shape(M)
    for j in range(k+1,m):
        M[k,j] *= -1
 
#-----------------------------------------------------------------------------
 
def ReverseRanksSpearmanMatrix(M, Lranks):
    '''
    Reverses (in-place) all the Spearman Correlations
    related to all variables from list Lranks.
 
    Parameters:
    -----------
        M: upper triangular matrix of Spearman Correlations.
        k: variable order to reverse on matrix M.
 
    Returns:
    --------
        M as a modified matrix.
 
    Example:
    --------
        >>> M = np.asmatrix(np.reshape(np.arange(1,50),(7,7)))
        >>> ReverseRanks(M,[1,3,5])
    '''
    for k in Lranks:
        ReverseRankSpearmanMatrix(M, k)
 
#-----------------------------------------------------------------------------
 
def AllRankCombinations(m):
    for k in range(2**(m)):
        b_ranks = list(np.binary_repr(k, width=m))[::-1]
        I       = np.array(b_ranks, dtype='int')
        Lranks  = [n for n,bit in enumerate(I) if bit == 1]
        #
        yield Lranks
 
#-----------------------------------------------------------------------------
 
def CorrelationSpearman(geneA, geneB, genesRanking, Corr):
    '''
    Computes Spearman Correlation between a pair of genes (geneA, geneB).
 
    Parameters:
    -----------
        genesRanking:
            DataFrame of Gene Ranking (Control, Disease, etc)
        Corr: dictionary of dictionaries of Correlations
              (keys are genes, values are correlations)
 
    Returns:
    --------
        Corr[geneA][geneB]: Spearman correlation coefficient between
                      genes geneA and geneB.
 
        Corr:   same Corr as input, but it may be modified.
                If the value Corr[geneA][geneB] does not exist yet,
                it will be added to the dictionary Corr.
                In this case, the dictionary Corr will be modified.
    '''
    if geneA not in Corr:
        Corr[geneA] = {}
    if geneB not in Corr:
        Corr[geneB] = {}
    #
    if geneB not in Corr[geneA]:

        gene_rankingA = genesRanking.loc[geneA]
        gene_rankingB = genesRanking.loc[geneB]
        #
        #calculates Spearman correlation
        rho,pval = spearmanr(gene_rankingA, gene_rankingB)
        #
        Corr[geneA][geneB] = rho
        Corr[geneB][geneA] = rho
    #
    return Corr[geneA][geneB]
 
#-----------------------------------------------------------------------------
 
def CreateSpearmanMatrix(Lgenes, genesRanking, Corr):
    m = len(Lgenes)
    M = np.asmatrix(np.zeros((m,m)))
    #
    # all combinations of elements from Lgenes
    for i in range(m-1):
        geneA = Lgenes[i]
        for j in range(i+1, m):
            geneB = Lgenes[j]
            #
            M[i,j] = CorrelationSpearman(geneA, geneB, genesRanking, Corr)
    #
    return M
 
#-----------------------------------------------------------------------------
 
def PathWeight_MaxKendall(path, genesRanking, Corr):
    '''
    Computes the maximum kendall concordance coefficient.
    A given path can have several edges from source to target,
    and MaxKendall of an edge is the best combination of the
    ranks in directed/reverse order.
    This function computes all possible combinations of
    reverse/directed order for the genes of a path and
    gives as result the one that gives the maximum
    Kendall concordance coefficient.
 
    Parameters
    ----------
        path: genes list whose Kendall's concordance will be computed.
        genesRanking:
            DataFrame of gene rankings (Control, Disease, etc)
            OBS: It *MUST* be in ranking format.
        Corr: dictionary of dictionaries of Correlations
              (keys are genes, values are correlations)
 
    Returns
    -------
        corrpath:   absolute correlation value of
                    a path and a target gene.
        Corr:   same Corr as input, but it may be modified.
                If the value of any correlation does not exist yet,
                it will be added to the dictionary Corr.
    '''
    #------------------------------------------------
    # Steps:
    #   1) create template matrix
    #   2) generate all combinations
    #   3) corr = CorrelationSpearman(geneA, geneB, genesRanking, Corr)
    #------------------------------------------------
    m = len(path)
    M = CreateSpearmanMatrix(path, genesRanking, Corr)
    #
    max_rho_av = 0.0 ### TO SOLVE -INF ####
    #
    for Lranks in AllRankCombinations(m-1):
        ReverseRanksSpearmanMatrix(M, Lranks)
        rho_av = ComputeAverageSpearmanMatrix(M)
        ### print max_rho_av, rho_av, Lranks
        max_rho_av = max(max_rho_av, rho_av)
        #
        # undo the reverses to restore the original matrix
        ReverseRanksSpearmanMatrix(M, Lranks)
    #
    W = (max_rho_av*(m-1) + 1) / m
    #
    return W
 
#-----------------------------------------------------------------------------
 
def all_shortest_paths(G, source, target, weight=None):
    """
    Produce all shortest paths from source to target starting at source.
 
    Parameters
    ----------
        G:      network graph
        source: initial node
        target: destination node
        weight: (optional)
                edges label of weight.
                If not provided, computes each edges with 1.
 
    Returns
    -------
        iterable that produces (yields) all shortest paths.
    """
    if weight is not None:
        pred,dist = nx.dijkstra_predecessor_and_distance(G,source,weight=weight)
    else:
        pred = nx.predecessor(G,source)
    if target not in pred:
        raise nx.NetworkXNoPath()
    stack = [[target,0]]
    top = 0
    while top >= 0:
        node,i = stack[top]
        if node == source:
            yield [p for p,n in reversed(stack[:top+1])]
        if len(pred[node]) > i:
            top += 1
            if top == len(stack):
                stack.append([pred[node][i],0])
            else:
                stack[top] = [pred[node][i],0]
        else:
            stack[top-1][1] += 1
            top -= 1
 
#-----------------------------------------------------------------------------
 
def AddPathToGraph(pathweight, path, G):
    '''
    Adds path to a graph by using relative importance of seeds
    combined with the weighted paths.
    It ***DOES NOT*** adds the SEEDS nodes.
    The importance of a node N relative to a seed S is:
        I(N,S) = lambda**(-dist_(N,S)).
    The pathweight of a path P is the Kendall's Concordance Coefficient
    of expression of all genes in P.
    These two values are multiplied and added to each gene from the paths.
 
    Parameters
    ----------
        pathweight: weight of the path.
        path:   path to be added to graph G.
        G:      Weighted Graph to which will be added the weighted path.
    '''
    #lambda, same value used in [White & Smith, 2003]
    l = 2
    #
    # adds pathweight to nodes
    nx.add_path(G, path)
    for d,n in enumerate(path[1:-1],1):
        nodeweight          = G.nodes[n].get('weight', 0.0)
        G.nodes[n]['weight'] = nodeweight + pathweight * (l**(-d))
    #
    # adds pathweight to edges
    n1 = path[1]
    for d,n2 in enumerate(path[2:-1],1):
        #edgeweight               = G.edge[n1][n2].get('weight', 0.0)
        #G.edge[n1][n2]['weight'] = edgeweight + pathweight * (l**(-d))
 
        edgeweight               = G[n1][n2].get('weight', 0.0)
        G[n1][n2]['weight'] = edgeweight + pathweight * (l**(-d))
        n1 = n2
 
#-----------------------------------------------------------------------------
 
def AddEPathsToGraph(eps, E_weights, E_paths, G, log):
    '''
    Adds a list of Espilon-shortest paths with its weights to a Graph.
    It assumes that all Epsilon-shortest paths belong to the same
    (source,target) pair.
    E_weights are normalized before they are added to the graph.
    To do this, each weighted is divided by |E_weighted_paths|.
    Obs: Epsilon is the maximum difference between the best paths.
 
    Parameters
    ----------
        E_weights:
            list of E_weights respectives to the E-shortets paths.
            It is computed the weighted mean, and then each weighted
            path is added to the graph G.
 
        E_paths: list of the E-shortest paths.
 
        G:  NetworkX Graph.
            Weighted Graph to which will be added the
            list of weighted E-shortets paths.
    '''
    min_weight = (1.0 - eps) * max(E_weights)
    #
    # removes paths below min_weight
    for i in range(len(E_paths), 0, -1):
        # if weight < (max_weight - epsilon)
        if E_weights[i-1] < min_weight:
            E_weights.pop(i-1)
            E_paths.pop(i-1)
        #else:
        #    break
    #
    # total of paths to normalize the weights
    total_paths = float(len(E_paths))
    # adds the normalized weights for the epsilon-shortest paths
    for i,path in enumerate(E_paths):
        weight = E_weights[i] / total_paths
        #-----------------------------------------
        # <<<INFO LEVEL>>>
        #log.info('\t>> %.6f\t%.6f\t%.0f\t%s' % ## <=== From here
        log.info('        >> %10.6f        %10.6f        %.0f        %s' %
                (E_weights[i],weight,total_paths,path))
        #-----------------------------------------
        E_weights[i] = weight
        AddPathToGraph(weight, path, G)
 
#-----------------------------------------------------------------------------

def EShortestPathsWeights(Tuple_procCD):
    '''
    Produce all paths in a depth-first-search starting at
    every source gene from Seeds, to every target gene
    from SuperSeeds list and finds the paths that produces
    the EPSILON best weight for each (source,target) pair.
 
    Parameters
    ----------
        epsilon: percentual of difference from maximum weight to accept a path
        G:       PPI interactome's Graph
        Seeds:   list of Seeds (putative causals) genes
        SuperSeeds:    list of (Seeds+Neighbors) genes
        genesRanking: DataFrame gene expression (Control, Disease, etc)
 
    Returns
    -------
        E_G:     NetworkX Graph.
                 Weighted Graph of the EPSILON-shortest paths
 
        info:    List of tuples with information about the paths
                 [(numpaths, dst_sp)]
    '''
    epsilon, G, Seeds, SuperSeeds, genesRanking, log, progress = Tuple_procCD
    log.info("EShortestPathsWeights\n")
    #
    E_G = nx.Graph()
    Corr = {}
    ########info = [] ################ <<< TO BE REMOVED >>>
    #
    total_peers  = len(Seeds) * (len(SuperSeeds) - 1)
    np_info = np.zeros((total_peers,3))
    current_peer = 0
    #
    for seed in Seeds:
        for neighbor in SuperSeeds:
            #----------------------------------------
            # <<<INFO LEVEL>>>
            progress.info('%f' % (current_peer / float(total_peers))) # progress bar

            log.info('%s %s' % (seed, neighbor))
            #----------------------------------------
            if seed == neighbor:
                continue
            arbpath = nx.shortest_path(G,seed,neighbor)
            dst_sp = len(arbpath) - 1
            #------------------------------------------------------------
            numpaths = 0
            E_paths   = []
            E_weights = []
            #------------------------------------------------------------
            # Computes all shortest paths
            path_iter = all_shortest_paths(G, seed, neighbor, weight=None)
            for path in path_iter:
                weight = PathWeight_MaxKendall(path,genesRanking,Corr)
                numpaths += 1
                pos = bisect.bisect(E_weights, weight)
                #
                E_weights.insert(pos, weight)
                E_paths.insert  (pos, path[:])
                #----------------------------------------
                # <<<DEBUG LEVEL>>>
                log.debug('%d        %.6f        %d        %s' % (numpaths,weight,pos,path))
                #----------------------------------------
            #
            AddEPathsToGraph(epsilon, E_weights, E_paths, E_G, log)
            log.info('')
            ################################################################
            ##info += [(len(E_paths), numpaths, dst_sp)] ### TO BE REMOVED
            ################################################################
            np_info[current_peer] = [len(E_paths), numpaths, dst_sp]
            current_peer += 1
    return E_G,np_info
 
#-----------------------------------------------------------------------------
 
def ProcessingPathSelection(epsilon,G,Seeds,SuperSeeds,C,D, log, progress):
    # C:  Control (expression)
    # D:  Disease (expression)
    #
    BestShortestPaths = EShortestPathsWeights
    #
    # Since it is using Kendall,
    # it must convert C,D to rankings.
    #print 'RankGeneExpression'
    rankingC = C.rank(axis=1)
    rankingD = D.rank(axis=1)
    #
    log.info('Seeds ({Size})\n\n{List}\n\n'.format
                  (Size=len(Seeds), List=Seeds))

    log.info('SuperSeeds ({Size})\n\n{List}\n\n'.format
                  (Size=len(SuperSeeds), List=SuperSeeds))
    #
    t1 = time.time()
    #
    #--------------------------------
    ############ logC e logD
    #
    args_procC = epsilon,G,Seeds,SuperSeeds,rankingC, log, progress
    args_procD = epsilon,G,Seeds,SuperSeeds,rankingD, log, progress
    Tuple_procCD = (args_procC, args_procD)

    with Pool(processes=2) as pool:
        log.info('\n')
        log.info('-----------------------------------------------')
        log.info('CONTROL')
        log.info('-----------------------------------------------')
        #
        log.info('\n')
        log.info('-----------------------------------------------')
        log.info('DISEASE')
        log.info('-----------------------------------------------')
        #
        results_procCD = pool.map(EShortestPathsWeights, Tuple_procCD)
        #
        #p.terminate() ################# <= FORCE PROCESS TERMINATION
        pool.close() # no more tasks
        pool.join()  # wrap up current tasks

    Tuple_GraphInfo_C,Tuple_GraphInfo_D  = results_procCD

    log.info('Process C finished.')
    log.info('Process D finished.')

    log.info('Final')
    #--------------------------------
    #
    t2 = time.time()
    #
    log.info('\n')
    log.info('-----------------------------------------------')
    log.info('TOTAL TIME(s): %f' % (t2-t1))
    log.info('-----------------------------------------------')
    #
    return (Tuple_GraphInfo_C, Tuple_GraphInfo_D)
 
#------------------------------------------------------------------------------
 
def StatsMatrix(Info, log):
    '''
    Prints statistical information about the paths.
 
    Parameters
    ----------
        Info: numpy matrix
              information matrix about the paths
              [EPaths, NumPaths, Dist_ShortestPaths]
    '''
    A = np.asarray(Info)
    log.info('        EPaths        NumPaths        Dist_SP')
    log.info('Min:        {}'.format(np.min(A,axis=0)))
    log.info('Mean:        {}'.format(np.mean(A,axis=0)))
    log.info('Median:        {}'.format(np.median(A,axis=0)))
    log.info('Max:        {}'.format(np.max(A,axis=0)))
    log.info('StdDev:        {}'.format(np.std(A,axis=0)))
    log.info('Sum:        {}'.format(np.sum(A,axis=0)))
 
#------------------------------------------------------------------------------

if __name__ == '__main__':
    print('path_selection main\n')
