# -*- coding: utf-8 -*-
#    Copyright (C) 2015-2020 by
#    Sérgio Nery Simões <sergionery@gmail.com>
#    All rights reserved.
#    BSD license.
#
# Author(s): Ana Rúbia R. Vicente <anarubiarvicente@gmail.com>,
#            Sérgio Nery Simões <sergionery@gmail.com>
#-----------------------------------------------------------------------------

"""
Constructs the Differential Network Visualization.

Input: files of a differential graph.

Output: file of Differential Network Visualization.
"""

#-----------------------------------------------------------------------------

import pandas as pd
import networkx as nx
#from networkx.readwrite import json_graph
#import matplotlib.pyplot as plt
#import json
#print (dir(json))

def interpolation(maxv, minv, value, maxsize, minsize):
    return minsize + ((value - minv)/(maxv - minv) * (maxsize - minsize))

#def interpolation_node(maxv, minv, value):
#    return 20 + ((value - minv)/(maxv - minv) * (80 - 20))
    
#def interpolation_edge(maxv, minv, value):
#    return 1 + ((value - minv)/(maxv - minv) * (10 - 1))

#------------------------------------------------------------------------------

# TODO: TO CHECK THIS THRESHOLD
def my_color(x):
    if (x > 0.25):
        return'#66ff99'
    elif (x < 0.25):
        return '#ff6666'
    else:
        return '#c2c2a3'

#------------------------------------------------------------------------------

def generate_network(args, topNodes=500):
    G = nx.Graph()

    ExpID     = args[0]
    filenodes = args[1]
    fileedges = args[2]

    nodes = pd.read_table(filenodes, index_col='NODE', sep='\t')
    nodes.sort_values(by='RankS', ascending=False, inplace=True)

    # getting TOP NODES order by 'S'
    nodes = nodes.head(topNodes)
    
    # X = (C+D) # Intensity Score
    max_node = nodes['X'].max()
    min_node = nodes['X'].min()
    print(max_node, min_node)

    edges =  pd.read_table(fileedges, sep="\t")
    max_edge = edges['X'].max()
    min_edge = edges['X'].min()
    
    
    # adding nodes to nodefile and setting features
    for i, row in nodes.iterrows():     
        color = my_color(float(row['Y']))
        #tam = interpolation(max_node, min_node, float(row[5]), 80, 20) 
        tam = interpolation(max_node, min_node, float(row['D']), 80, 20) 

        G.add_node(row.name,
            graphics={'type': 'ellipse', 'fill': '#ff9999',
                      'outline': '#666666', 'fill': color,
                      'w': tam, 'h': tam, 'width': 1},
            LabelGraphics={'fontSize': int(0.5 * tam), 'text': row.name})

    # adding edges to edgefile and setting features
    for i, row in edges.iterrows():     
        attributes=row[1:]   


        node1 = [row.name, row['NODE1']][1]
        node2 = [row.name, row['NODE2']][1]

        #ed = row[2].replace(")", "").replace("(", "").replace("'", "").split(", ")
        color = my_color(float(row['Y']))
        tam = interpolation(max_edge, min_edge, float(row['X']), 10, 1)

        if (node1 in G.nodes() and node2 in G.nodes()):
            G.add_edge(node1, node2, weight=0.1, graphics={'width': tam, 'fill': color, 'type': 'line'})
                  
        
        # ed = [row.name, row['EDGE']]
        # #ed = row[2].replace(")", "").replace("(", "").replace("'", "").split(", ")
        # color = my_color(float(row['Y']))
        # tam = interpolation(max_edge, min_edge, float(row['X']), 10, 1)

        # if (ed[0] in G.nodes() and ed[1] in G.nodes()):
        #     G.add_edge(ed[0], ed[1], weight=0.1, graphics={'width': tam, 'fill': color, 'type': 'line'})
        
    '''
    Choosing Network Layout...
    '''
    #pos = nx.circular_layout(G)
    #pos = nx.shell_layout(G)
    #pos = nx.spring_layout(G)
    #pos = nx.spectral_layout(G)
    pos = nx.random_layout(G) 
    #pos = nx.kamada_kawai_layout(G) #legal #SOH FUNCIONA NA VERSAO 2.2 -- ESTOU USANDO A 1.9
    #pos = nx.fruchterman_reingold_layout(G)
    
    '''
    Applying Network Layout:
    for each node is defined the position (x,y).
    '''
    for node in  G.nodes(data=True): 
        node[1]['graphics']['x'] = pos[node[0]][0] * 500
        node[1]['graphics']['y'] = pos[node[0]][1] * 500

    filenetwork = filenodes.replace('3_NODES.txt', '') + \
                                '4_CYTOSCAPE-top_%d.gml' % (topNodes)

    #------------------------------------------------------------------------------

    # saving the network file (GML format) 
    nx.write_gml(G, filenetwork)

    return 0
