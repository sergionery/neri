# -*- coding: utf-8 -*-
#    Copyright (C) 2015-2018 by
#    Sérgio Nery Simões <sergionery@gmail.com>
#    All rights reserved.
#    BSD license.
#
# Author: Sérgio Nery Simões <sergionery@gmail.com>

"""
Performs the differential analyses between the two weighted graphs,
Control and Disease Graphs (GraphC and GraphD), in order to generate
the differential network.
"""

import pandas as pd
import numpy as np
import networkx as nx

from scipy.optimize import curve_fit

import sys

import matplotlib.pyplot as plt
import plotly.express as px
import plotly.graph_objects as go

#------------------------------------------------------------------------------
#Functions
#------------------------------------------------------------------------------

def f_powerlaw(x, a, k):
    return a*(x)**(-k)

#------------------------------------------------------------------------------

def f_exp(x, a, b):
    return a*np.exp(-b*x)

#------------------------------------------------------------------------------
# Differential Analyses
#------------------------------------------------------------------------------

def GetPercentilePoints(X, Y, window, slide, percentile, x_ini, x_end):
    '''
    Get percentile points to be used in the fitting curve,
    i.e. for each X, take only the max(abs(Y)) values.
    Obs: only points where X is within [x_ini, x_end] are returned.

    Parameters
    ----------
    X, Y:       numpy vectors.
                (X,Y) coordinates of the points.
                X and Y must have the same size.

    window:     float, value.
                size of window.

    slide:      float, value.
                size of window's slides.

    percentile: float, value.
                Percentile of points to be returned.

    x_ini:      float.
    x_end:      float.
                limits, only points where X is within [x_ini, x_end] are returned.


    Returns
    -------
    Xabs, Yabs: numpy vectors containing selected points
                by its percentile.
    '''
    Xabs, Yabs = [], []
    #
    half_window = window / 2.0
    n_slides = int((x_end - x_ini) / slide)
    xspace = np.linspace(x_ini + half_window, x_end - half_window,
                         n_slides, endpoint = False)
    for xp in xspace:
        indX = np.logical_and(X >= xp-half_window, X < xp+half_window)
        # if window is not empty
        if indX.any():
            Ycurr = np.abs(Y[indX])
            yp = np.percentile(Ycurr, percentile)
            #
            Xabs += [xp]
            Yabs += [yp]
    #
    Xabs = np.asarray(Xabs)
    Yabs = np.asarray(Yabs)
    #
    return Xabs, Yabs

#------------------------------------------------------------------------------

def PercentileCurveFitting(X, Y, func, window, slide, percentile, x_ini, x_end):
    '''
    Returns the Curve Fitting parameters to a given percentile of points.
    Obs: only points where X is within [x_ini, x_end] are returned.

    Parameters
    ----------
    X, Y:       numpy vectors.
                (X,Y) coordinates of the points.
                X and Y must have the same size.

    func:       function.
                power-law(default) or exponential could be used.

    percentile: float, value.
                Percentile of points to be returned.

    window:     float, value.
                size of window.

    slide:      float, value.
                size of window's slides.

    x_ini:      float.
    x_end:      float.
                limits, only points where X is within [x_ini, x_end] are returned.

    Returns
    -------
    Xabs, Yabs: numpy vectors containing selected points
                by its percentile.
    '''
    #
    x_ini = float(min(X) if x_ini is None else x_ini)
    x_end = float(max(X) if x_end is None else x_end)
    #
    Xabs, Yabs = GetPercentilePoints(X, Y, window, slide, percentile, x_ini, x_end)
    # curve fitting
    param_fit, param_cov = curve_fit(func, Xabs, Yabs)
    #
    return param_fit, param_cov

#------------------------------------------------------------------------------

def NodesEdges2Graph(dictNodes, dictEdges, weight='weight'):
    '''
    Deprecated:
    to be used only in case weigths of Nodes and Edges was saved into a file.
    '''
    G = nx.Graph()
    for node in dictNodes:
        G.add_node(node, {weight: dictNodes[node]})
    #
    for edge in dictEdges:
        G.add_edge(edge[0], edge[1], {weight: dictEdges[edge]})
    #
    return G


#------------------------------------------------------------------------------

def GetNodesWeight(G):
    '''
    Get the weight of nodes from a weight Graph G in a given condition.

    Parameters
    ----------
        G:  networkx graph
            graph with nodes/edges containing label 'weight'
            with its respective weight.
    Returns
    -------
        weight_nodes:   dictionary of node weights.
                        {node: weight}
    '''
    weight_nodes = {}
    for n in G.nodes():
        weight = G.nodes[n].get('weight',0)
        if weight > 0:
            weight_nodes[n] = weight
    #
    return weight_nodes

#------------------------------------------------------------------------------

def GetEdgesWeight(G):
    '''
    Get the weight of edges from a weight Graph G.

    Parameters
    ----------
        G:  networkx graph
            graph with nodes/edges containing label 'weight'
            with its respective weight.
    Returns
    -------
        weight_edges: dictionary of edge tuples (u,v) weights.
                    {(u,v): weight}, where u,v are nodes.
    '''
    weight_edges = {}
    for n0,n1 in G.edges():
        weight = G[n0][n1].get('weight',0)
        if weight > 0:
            # store edges in order
            (u,v) = (n0,n1) if n0 <= n1 else (n1,n0)
            weight_edges[(u,v)] = weight
    #
    return weight_edges
#------------------------------------------------------------------------------

def MergeNodesWeights(GraphC, GraphD, weight='weight'):
    '''
    Create a DataFrame by merging NODES weights from Graphs C and D.

    Parameters
    ----------

    GraphC,
    GraphD:     NetworkX weighted Graphs (Control and Disease).

    weight:     string of weight parameter used in the graphs.

    Returns
    -------

    dfScores:   DataFrame.
                Columns [C,D] of NODES WEIGHTS.
    '''
    weightsC = GetNodesWeight(GraphC)
    weightsD = GetNodesWeight(GraphD)
    #
    setC   = set(weightsC.keys())
    setD   = set(weightsD.keys())
    keyslist = sorted(setC | setD)
    #
    sigmaC = pd.Series(weightsC, index=keyslist)
    sigmaD = pd.Series(weightsD, index=keyslist)
    #
    sigmaC.fillna(0.0, inplace=True)
    sigmaD.fillna(0.0, inplace=True)
    #
    dataweights = {'C':sigmaC, 'D':sigmaD}
    dfNodesWeights = pd.DataFrame(data=dataweights, columns=['C','D'])
    #
    return dfNodesWeights

#------------------------------------------------------------------------------

def MergeEdgesWeights(GraphC, GraphD, weight='weight'):
    '''
    Create a DataFrame by merging EDGES weights from Graphs C and D.

    Parameters
    ----------

    GraphC,
    GraphD:     NetworkX weighted Graphs (Control and Disease).

    weight:     string of weight parameter used in the graphs.

    Returns
    -------

    dfScores:   DataFrame.
                Columns [C,D] of EDGES WEIGHTS.
    '''
    weightsC = GetEdgesWeight(GraphC)
    weightsD = GetEdgesWeight(GraphD)
    #
    setC   = set(weightsC.keys())
    setD   = set(weightsD.keys())
    keyslist = sorted(setC | setD)
    #
    sigmaC = pd.Series(weightsC, index=keyslist)
    sigmaD = pd.Series(weightsD, index=keyslist)
    #
    sigmaC.fillna(0.0, inplace=True)
    sigmaD.fillna(0.0, inplace=True)
    #
    dataweights = {'C':sigmaC, 'D':sigmaD}
    dfEdgesWeights = pd.DataFrame(data=dataweights, columns=['C','D'])
    #
    return dfEdgesWeights

#------------------------------------------------------------------------------

def InsertDefaultScores(dfWeights, func=f_powerlaw, param_fit=None):
    '''
    Uses Weights DataFrame to compute Scores columns.
    '''
    #
    sigmaC = dfWeights['C']
    sigmaD = dfWeights['D']
    #
    X = (sigmaC + sigmaD)
    Y = (sigmaD - sigmaC) / X
    #
    dfWeights['X']  = X
    dfWeights['Y']  = Y
    #
    # RETROCOMPATIBILITY
    if param_fit is None:
        # CANONICAL POWER-LAW
        param_fit = np.array([1.0, 1.0])
    #
    Yf = func(X, *param_fit)
    #
    Score_Delta_norm = abs(Y / Yf)
    #
    dfWeights['Yf'] = Yf
    dfWeights['S']  = Score_Delta_norm
    #
    dfWeights['RankX'] = dfWeights['X'].rank(ascending=False)
    dfWeights['RankS'] = dfWeights['S'].rank(ascending=False)
    #
    return dfWeights

#------------------------------------------------------------------------------

def WriteScores(GraphC, GraphD, prefix, weight='weight'):
    '''
    Write Nodes and Edges Weights to files.
    This step is the Relative Importance and this output 
    will be used by Differential Analyses step.
    '''
    fileNodes = prefix + '3_NODES.txt'
    fileEdges = prefix + '3_EDGES.txt'

    dfNodesWeights = MergeNodesWeights(GraphC, GraphD, weight=weight)
    dfEdgesWeights = MergeEdgesWeights(GraphC, GraphD, weight=weight)

    dfScoresNodes = InsertDefaultScores(dfNodesWeights)
    dfScoresEdges = InsertDefaultScores(dfEdgesWeights)

    dfScoresNodes.to_csv(fileNodes, sep='\t', index=True, index_label='NODE')
    dfScoresEdges.to_csv(fileEdges, sep='\t', index=True, index_label=['NODE1', 'NODE2'])

    return fileNodes, fileEdges

#------------------------------------------------------------------------------

def InsertFitScores(dfWeights, top, func, param_fit, x_ini, x_end):
    #
    dfWeights = InsertDefaultScores(dfWeights, func, param_fit)
    #
    dfSorted, top, Bi, Ci, Di, Zi = ClassifyPoints(dfWeights, top, x_ini, x_end)
    #
    return dfSorted, top, Bi, Ci, Di, Zi

#------------------------------------------------------------------------------

def ClassifyPoints(dfScores, top, x_ini=None, x_end=None):
    """
    Returns 3 series to be colored: Zi (gray), Ci (green), Di (red).

    Parameters:
    -----------
        dfScores: dataframe,
                  scores of points (could be nodes or edges);
                  must be sorted by 'S'.

        top:     int, number of points to be classified
                       (colored in Red or Green);

    Returns:
    --------
        Zi:     Series, points to be colored in gray  (Unselected).
        Ci:     Series, points to be colored in green (Control altered).
        Di:     Series, points to be colored in red   (Disease altered).

        top:    int, number of top genes/edges to be selected 
                as altered in control/disease.

        dfSorted: Dataframe, sorted by 'S' with a new column of class.
    """
    dfSorted = dfScores.sort_values(['S'], ascending=0)
    #
    x_ini = float(dfSorted.min()['X'] if x_ini is None else x_ini)
    x_end = float(dfSorted.max()['X'] if x_end is None else x_end)
    #
    X = dfSorted['X']
    Y = dfSorted['Y']
    S = dfSorted['S']
    #
    Xrange = (X >= x_ini) & (X <= x_end)
    count  = Xrange.value_counts()[True]
    #
    top = count if count < top else top
    limit = dfSorted[Xrange]['S'].iloc[top-1]
    #
    Zi = (S <  limit) | (~Xrange)
    Bi = (S >= limit) & ( Xrange)  # Both C and D indices
    Ci = (Bi) & (Y < 0)
    Di = (Bi) & (Y > 0)
    #
    category = Ci*1+Di*2+Zi*3
    dfSorted['CLASS'] = pd.Series(category).map({1:'Control', 2:'Disease', 3:'Unselected'})
    #
    return dfSorted, top, Bi, Ci, Di, Zi

#------------------------------------------------------------------------------

def ComputeParamSel(dfSortedSel, top, func, param_fit):
    '''
    Return new param of the top points.
    '''
    a_fit, b_fit  = param_fit
    #
    x_sel  =     dfSortedSel['X'].iloc[top-1]
    y_sel  = abs(dfSortedSel['Y'].iloc[top-1])
    y_fit  = func(x_sel, *param_fit)
    #
    a_sel  = a_fit * (y_sel / y_fit)
    #
    return np.asarray([a_sel, b_fit])

#------------------------------------------------------------------------------

def SetPlotParams(language='en', function='power_law',
                  keyname='Genes', prefix = 'fig'):
    # Defines font
    font = {'family' : 'serif',
            'color'  : 'darkred',
            'weight' : 'normal',
            'size'   : 22,
            }
    #
    l = 0 if language=='en' else 1  # 1: pt
    f = 0 if function=='power_law' else 1 # 1: exponential
    # English, Portuguese
    Clabel = (u'Control %s',\
              u'%s Controle')[l] % keyname
    Dlabel = (u'Disease %s',\
              u'%s Doença')[l] % keyname
    functext = {0:(u'Power-law:\n$f(x)=%.5f*x^{-%.5f}$',\
                   u'Lei-de-potência:\n$f(x)=%.5f*x^{-%.5f}$'),
                1:(u'Exponential:\n$f(x)=%.5f*x^{-%.5f}$',\
                   u'Exponencial:\n$f(x)=%.5f*x^{-%.5f}$')}[f][l]
    title   = (u'Selected %s ',\
               u'Seleção de %s ')[l] % keyname
    filestr = (prefix+'-EN',\
               prefix+'-PT')[l]
    #
    # Defines figure params
    fig = { 'xlabel': u'$X=(\sigma_C+\sigma_D)$',
            'ylabel': u'$\Delta=(\sigma_D-\sigma_C) / (\sigma_C+\sigma_D)$',
            'Clabel': Clabel,
            'Dlabel': Dlabel,
            'title'   : title,
            'functext': functext,
            'filename': filestr
            }
    #
    #Defines function params
    func = (f_powerlaw, f_exp)[f]
    # obs: f_exp interpolation doesnt work for x_ini > 1
    #
    return font, fig, func

#------------------------------------------------------------------------------

def VolcanoPlot_matplotlib(dfScores, top, func, param_fit,
                fig, font, x_ini, x_end,
                plot_selection = True,
                Met_i=None, Met_label='',
                save_figure=False):
    #
    dfS, top, Bi, Ci, Di, Zi = ClassifyPoints(dfScores, top, x_ini, x_end)
    #
    # dfS[Bi]: only valid points (x_ini <= X <= x_end)
    #          and altered in Both conditions (control or disease)
    param_sel = ComputeParamSel(dfS[Bi], top, func, param_fit)
    #
    X = dfS['X']
    Y = dfS['Y']
    # limit points
    xlim = (100*(int(max(X)/100)+1) if x_end is None else x_end)
    y0 = 0.0
    #
    Xf =  np.logspace(-3, np.log10(xlim), num=100)
    # Adjusted curve for PERCENTUAL SELECTION
    Yf     = func(Xf, *param_sel)
    #
    plt.figure(figsize=(24,12))
    #
    plt.rc('xtick', labelsize=22)
    plt.rc('ytick', labelsize=22)
    #
    # select X range to plot
    plt.axis([0.0, xlim, -1.1, 1.1])
    #
    if fig['functext'] != '':
        functext = fig['functext'] % (param_sel[0],param_sel[1])
        plt.text(0.60*xlim, 0.60, functext, fontdict=font)
    #
    plt.title ('%s (%d)' % (fig['title'], top), fontdict=font)
    plt.xlabel(fig['xlabel'], fontdict=font)
    #plt.ylabel(r'$\frac{(D-C)}{C+D}$', fontdict=font)
    plt.ylabel(fig['ylabel'], fontdict=font)
    #
    if plot_selection == True:
        plt.plot(X[Zi], Y[Zi], color='gray', linestyle='None', marker='.', label='')
        plt.plot(X[Ci], Y[Ci], 'go', markersize=8, label=fig['Clabel'])
        plt.plot(X[Di], Y[Di], 'ro', markersize=8, label=fig['Dlabel'])
    else:
        plt.plot(X[Zi],Y[Zi],color='blue', linestyle='None', marker='o')
        plt.plot(X[Ci],Y[Ci],color='blue', linestyle='None', marker='o')
        plt.plot(X[Di],Y[Di],color='blue', linestyle='None', marker='o')
    #
    if Met_i is not None:
        #plt.plot(X[Met_i],Y[Met_i],'bo', label=Met_label)
        MetC = sorted(set(Met_i) & set(Ci))
        MetD = sorted(set(Met_i) & set(Di))
        MetZ = sorted(set(Met_i) & set(Zi))
        plt.plot(X[MetC],Y[MetC],color='g',marker='o',markersize=8,ls='',fillstyle='bottom', markerfacecoloralt=(0.1,0.1,1.0))
        plt.plot(X[MetD],Y[MetD],color='r',marker='o',markersize=8,ls='',fillstyle='top',    markerfacecoloralt=(0.1,0.1,1.0))
        plt.plot(X[MetZ],Y[MetZ],color=(0.1,0.1,1.0),marker='o', ls='', label=Met_label)
    #
    # lines
    plt.plot([0,xlim],[y0,y0], linestyle='dashed', color='0.5')
    plt.plot([x_ini, x_ini],[-1.1, 1.1], linestyle='dashed', color='0.5')
    plt.plot([x_end, x_end],[-1.1, 1.1], linestyle='dashed', color='0.5')
    #
    if plot_selection == True:
        # plot decision surface
        plt.plot(Xf, +Yf, color='k', linestyle='dashed')
        plt.plot(Xf, -Yf, color='k', linestyle='dashed')
    #
    plt.legend()
    #
    # Tweak spacing to prevent clipping of ylabel
    plt.subplots_adjust(left=0.15)
    #
    if save_figure == True:
        plt.savefig(fig['filename']+'.pdf', format='pdf')
        plt.close()
    else:
        plt.show()

#------------------------------------------------------------------------------

def VolcanoPlot_plotly(dfScores, top, func, param_fit,
                fig_arg, font, x_ini, x_end,
                plot_selection = True,
                Met_i=None, Met_label='',
                save_figure=False, keyname=[]):
    #
    dfS, top, Bi, Ci, Di, Zi = ClassifyPoints(dfScores, top, x_ini, x_end)
    #
    # dfS[Bi]: only valid points (x_ini <= X <= x_end)
    #          and altered in Both conditions (control or disease)
    param_sel = ComputeParamSel(dfS[Bi], top, func, param_fit)
    #
    X = dfS['X']
    Y = dfS['Y']
    # limit points
    xlim = (100*(int(max(X)/100)+1) if x_end is None else x_end)
    y0 = 0.0
    #
    #Xf =  np.logspace(-3, np.log10(xlim-x_ini), num=100)    ###################
    Xf =  np.logspace(-3, np.log10(xlim), num=100)
    # Adjusted curve for PERCENTUAL SELECTION
    Yf     = func(Xf, *param_sel)

    dfS['SIZE'] = Bi*10 + Zi*1

    fig = px.scatter(dfS, x="X", y="Y", size='SIZE', size_max=8,
                    color="CLASS", range_y=[-1.05,1.05],
                    color_discrete_map={'Control':'green', 
                                         'Disease':'red', 
                                         'Unselected':'gray'},
                    hover_name=keyname,
                    hover_data={'X': True, 'Y': True, 'S':True,
                                'RankX': True, 'RankS':True,
                                'CLASS': False, 'SIZE': False},

                                         )

    fig.update_layout(title={'text': '%s (%d)' % (fig_arg['title'], top),
                            'y': 0.95,
                            'x': 0.50,
                            'xanchor': 'center',
                            'yanchor': 'top'})

    fig.add_trace(go.Scatter(x=Xf, y=Yf, mode="lines",
            line=go.scatter.Line(color="blue",dash='solid', width=0.25),
            showlegend=False) )

    fig.add_trace(go.Scatter(x=Xf, y=-Yf, mode="lines",
            line=go.scatter.Line(color="blue",dash='solid', width=0.25),
            showlegend=False) )

    fig.add_trace(go.Scatter(x=[x_ini,x_ini], y=[-1.0, 1.0], mode="lines",
            line=go.scatter.Line(color="blue",dash='solid', width=0.25),
            showlegend=False) )
    #
    if save_figure == True:
        fig.write_html(fig_arg['filename']+'.html')
    else:
        fig.show()

    

#------------------------------------------------------------------------------

def DifferentialAnalyses(dfWeights, top, x_quantile=0.0,
                        # Parameters
                        func       = f_powerlaw,
                        window     = 2,
                        slide      = 1,
                        percentile = 75,
                        x_ini      = None,
                        x_end      = None):
    '''
    Computes Scores from weights (Control and Disease).
    Returns a dataframe containing Scores [C,D,X,Y,Yf,S].
    Also returns the fit parameters used to compute Yf and S.

    Parameters
    ----------

    dfWeights:  DataFrame containing the weights (C,D).

    x_quantile: float, value.
                quantile of the X cutoff (x_ini) used to compute x_ini,
                which is used to filter out points whose have values
                X <= x_ini. Typical value of x_quantile is 0.95.
                Obs: if x_quantile is NOT NAN then x_ini will be
                computed from its quantile value and then x_ini
                value inputed at this function will not be used.

    func:       function.
                power-law(default) or exponential could be used.

    window:     float, value.
                size of window.

    slide:      float, value.
                size of window's slides.

    percentile: float, value.
                Percentile of points to be returned.

    x_ini:      float. (Obs: only used as input if x_quantile is NAN).
    x_end:      float.
                limits, only points where X is within [x_ini, x_end] are returned.

    Returns
    -------

    dfScores:   dataframe.
                containing Scores [C,D,X,Y,Yf,S] of Nodes/Edges.
                C,D:    sigmaC,sigmaD (control and disease)
                X:      C+D
                Y:      (D-C)/X, also called Delta
                Yf:     f(X), fit function
                S:      abs(Y)/Yf, also called Delta' (normalized Delta)
                        it measures the difference between (C)ontrol and (D)isease.

    param_fit:  pameters which fitted the function.
    '''
    sigmaC = dfWeights['C']
    sigmaD = dfWeights['D']
    #
    X = (sigmaC + sigmaD)
    Y = (sigmaD - sigmaC) / X
    #
    dfWeights['X']  = X
    dfWeights['Y']  = Y
    #
    if x_quantile==0.0:
        x_ini = 0
    elif x_quantile==x_quantile: # if is not NAN
        x_ini = dfWeights['X'].quantile(x_quantile)
    else:
        x_ini = 5.0 # compatibility (validation with old results)
    #
    #====================================
    # The function call below was kept
    # only for retrocompatibility purposes.
    # From now on it will be used the 
    # canonical form of power-law function.
    # f(x) = a*x**(-k) = 1/x; (a,b)=(1,1)
    #
    # DEPRECATED:
    # x_ini, x_end and x_quant are all 
    # obsoletes and will be removed in
    # the next version.
    #====================================
    param_fit, param_cov = \
                    PercentileCurveFitting(X, Y, func,
                                        window, slide, percentile,
                                        x_ini, x_end)
    #
    #====================================
    # Canonical Power-Law (1/x)
    if x_ini == 0:
        param_fit[0] = 1.0
        param_fit[1] = 1.0
    #====================================
    #
    dfSorted, top, Bi, Ci, Di, Zi = \
                    InsertFitScores(dfWeights, top, func, param_fit,
                                        x_ini, x_end)
    #
    return dfSorted, param_fit, x_ini, top

#------------------------------------------------------------------------------
# MAIN
#------------------------------------------------------------------------------

def main():
    #------------------------------------------------------------------------------
    # PARAMETERS (INPUT)
    #------------------------------------------------------------------------------
    # Parameters (Selection)
    #------------------------------------------------------------------------------
    # Top N genes or edges to be selected
    topNodes   = int(sys.argv[1])
    topEdges   = int(sys.argv[2])

    # Nodes and Edges filenames
    filenodes  = sys.argv[3]
    fileedges  = sys.argv[4]

    # DEPRECATED: 
    # Quantile of X to compute x_ini cutoff to be filtered out;
    # Recomended to use 0.00;
    # Obs: use NAN to Xini=5.0 to validation with old results.
    x_quant_Nodes = float(sys.argv[5])
    x_quant_Edges = float(sys.argv[6])

    #------------------------------------------------------------------------------
    # Parameters (Sliding Window)
    #------------------------------------------------------------------------------
    func       = f_powerlaw
    window     = 2
    slide      = 1
    percentile = 75
    x_ini      = None   # used only x_quant == NAN
    x_end      = None   # Not used

    #------------------------------------------------------------------------------
    # RELATIVE IMPORTANCE OUTPUT
    #------------------------------------------------------------------------------

    # Write Relative Importance
    #WriteScores(GraphC, GraphD, weight='weight')

    #------------------------------------------------------------------------------
    #
    # DIFFERENTIAL NETWORK ANALYSES
    #
    #------------------------------------------------------------------------------

    #------------------------------------------------------------------------------
    # READS WEIGHTS FILE (RELATIVE IMPORTANCE)
    #------------------------------------------------------------------------------
    # NODES
    #
    dfNodesWeights = pd.read_table(filenodes, index_col='NODE')
    #------------------------------------------------------------------------------
    # EDGES
    #
    header = 'NODE1\tNODE2\tC\tD\tX\tY\tYf\tS\tRankX\tRankS'.split()
    dfEdgesWeights = pd.read_table(fileedges, header=0, names=header, index_col=[0,1])
    #------------------------------------------------------------------------------

    #--------------------------------
    # NODES: DIFFERENTIAL ANALYSIS
    #--------------------------------
    dfScoresNodes, paramNodes, \
        x_ini_Nodes, topNodes = \
            DifferentialAnalyses(dfNodesWeights, topNodes, x_quant_Nodes,
                                 func, window, slide, percentile, x_ini, x_end)
    #--------------------------------
    # EDGES: DIFFERENTIAL ANALYSIS
    #--------------------------------
    dfScoresEdges, paramEdges, \
        x_ini_Edges, topEdges = \
            DifferentialAnalyses(dfEdgesWeights, topEdges, x_quant_Edges,
                                 func, window, slide, percentile, x_ini, x_end)

    #--------------------------------
    # WRITES DIFFERENTIAL ANALYSES TO FILE
    #--------------------------------

    prefixNodes = filenodes.replace('3_NODES.txt', '') + \
                                    '4_NODES-xi%02.0f-top_%d' % (x_ini_Nodes*100, topNodes)

    prefixEdges = fileedges.replace('3_EDGES.txt', '') + \
                                    '4_EDGES-xi%02.0f-top_%d' % (x_ini_Edges*100, topEdges)

    dfScoresNodes.reset_index(inplace=True)
    dfScoresNodes.to_csv(prefixNodes + '.txt', sep='\t', index=False)

    dfScoresEdges.reset_index(inplace=True)
    dfScoresEdges.to_csv(prefixEdges + '.txt', sep='\t', index=False)

    #------------------------------------------------------------------------------
    #
    # PLOTTING (Matplotlib + Plotly)
    #
    #------------------------------------------------------------------------------

    #--------------------------------
    # NODES SELECTION (VOLCANO PLOT)
    #--------------------------------

    font, fig, func = SetPlotParams(language='en', function='power_law', 
                                    keyname='NODES', prefix = prefixNodes)

    VolcanoPlot_matplotlib(dfScoresNodes, topNodes, func, paramNodes, \
                fig, font, x_ini_Nodes, x_end, plot_selection = True,\
                Met_i=None, Met_label='', save_figure=True)

    VolcanoPlot_plotly(dfScoresNodes, topNodes, func, paramNodes, \
                fig, font, x_ini_Nodes, x_end, plot_selection = True,\
                Met_i=None, Met_label='', save_figure=True, keyname='NODE')

    #--------------------------------
    # EDGES SELECTION (VOLCANO PLOT)
    #--------------------------------

    font, fig, func = SetPlotParams(language='en', function='power_law', 
                                    keyname='EDGES', prefix = prefixEdges)

    VolcanoPlot_matplotlib(dfScoresEdges, topEdges, func, paramEdges, \
                fig, font, x_ini_Edges, x_end, plot_selection = True,\
                Met_i=None, Met_label='', save_figure=True)

    dfScoresEdges['EDGE'] = dfScoresEdges['NODE1'] + ' <-> ' + dfScoresEdges['NODE2']
    VolcanoPlot_plotly(dfScoresEdges, topEdges, func, paramEdges, \
                fig, font, x_ini_Edges, x_end, plot_selection = True,\
                Met_i=None, Met_label='', save_figure=True, keyname='EDGE')

#------------------------------------------------------------------------------
#------------------------------------------------------------------------------

# To run (using x_quantile=0.95, for example):
# python differential_analyses.py 100 500 \
#               LUNGS_COAG_20200812_191025_E05-3_NODES.txt\
#               LUNGS_COAG_20200812_191025_E05-3_EDGES.txt\
#               0.00 0.00

# To run (using x_ini=5, older version, x_quantile must be NAN):
# python differential_analyses.py 100 500\
#               LUNGS_COAG_20200812_191025_E05-3_NODES.txt\
#               LUNGS_COAG_20200812_191025_E05-3_EDGES.txt\
#               NAN NAN

if __name__ == '__main__':
    main()

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# TEST (Validation)
#------------------------------------------------------------------------------
# import pandas as pd
# import numpy as np

# # NODES
# N1 = pd.read_table('LUNGS_COAG_20200812_191025_E05-3_NODES.txt', index_col='NODE')
# N2 = pd.read_table('LUNGS_COAG_20200812_191025_E05-4_NODES_xi500.txt', index_col='NODE')

# np.sum(np.abs(N1 - N2))
# np.count_nonzero(N1[:1000].index != N2[:1000].index)
# N1[N1.index != N2.index]
# N2[N1.index != N2.index]

# # EDGES
# header = 'NODE1\tNODE2\tC\tD\tX\tY\tYf\tS\tRankX\tRankS'.split()
# E1 = pd.read_table('LUNGS_COAG_20200812_191025_E05-3_EDGES.txt', header=0, names=header, index_col=[0,1])
# E2 = pd.read_table('LUNGS_COAG_20200812_191025_E05-4_EDGES_xi500.txt', header=0, names=header, index_col=[0,1])

# np.sum(np.abs(E1 - E2))
# np.count_nonzero(E1[:1000].index != E2[:1000].index)
# E1[E1.index != E2.index]
# E2[E1.index != E2.index] 



