MINT simple tab-delimited file format:


Columns:
protein_xref_1	protein_xref_2	alternative_identifiers_1	alternative_identifiers_2	protein_alias_1	protein_alias_2	detection_method	author_name	pmid	protein_taxid_1	protein_taxid_2	interaction_type	source_database_id	confidence	bioSource_taxid:celltype:tissue	figures	url	negative	experiment_role_1	biological_role_1	expression_level_1	protein_identification_1	sample_process_1	experimental_preparation_1	binding_sites_1	modifications_1	mutations_1	experiment_role_2	biological_role_2	expression_level_2	protein_identification_2	sample_process_2	experimental_preparation_2	binding_sites_2	modifications_2	mutations_2

All proteins involved in an interaction are represented on a single line. 
If the experimental role of one of the protein is bait, it will be inserted in first column 
and all preys will be added in the second one. If all participant are neutral component, one
protein is chosen casually and added in the first column, all other proteins are in the second one.

columns;
ID interactor A (bait)	ID interactors B (all preys)	Alias interactor A (bait)	Alias(es) interactors B (preys)	Taxid interactor A (bait)	Taxid(s) interactors B (preys)	Experimental role A (bait)	Experimental role B (preys)	Interaction detection method(s)	Publication Identifier(s)	Interaction type(s)	Interaction identifier(s)	bioSource_taxid	negative	mint protein group A  (taxon)	mint protein group B (taxon)	MINT confidence score



files are available for the full data or for only interaction involving one organism group:

full-mitab.txt
caenorhabditis-mitab.txt
drosophila-mitab.txt
mammalia-mitab.txt
saccharomyces-mitab.txt
viruses-mitab.txt



Columns 1-14 are defined in the MITAB, all columns are not used. Others columns have been added by MINT.


	1. ID interactor A (bait)
	  Unique identifier for first interactor (bait), represented as dbName:ac	
      where dbName is the name of the corresponding database as defined
      in the PSI MI controlled vocabulary, and ac is the unique primary
      identifier of the molecule in the database. Identifiers from
      multiple databases can be separated by ‘|’. Proteins should be
      identified by their UniProt and/or RefSeq accession number, as
	2. ID interactors B (all preys)
	  Unique identifier for all preys separated by ';', represented as dbName:ac	
      where dbName is the name of the corresponding database as defined
      in the PSI MI controlled vocabulary, and ac is the unique primary
      identifier of the molecule in the database.
	
	3. Alias interactor A (bait)
	  Alternative identifier for interactor A, for example the official
      gene symbol as defined by e.g. HUGO. Representation as
      databaseName:identifier. Only one alias is given
	4. Alias(es) interactors B (preys)
	  Alternative identifier for preys, separated by ';'.
	5. Taxid interactor A (bait)
      taxid taken from the PSI MI controlled vocabulary. Representation
      as databaseName:identifier(organism name).
      Note: In this column, the databaseName:identifier notation is only
      there for consistency. Currently no taxonomy identifiers other
      than NCBI taxid are anticipated.
	6. Taxid(s) interactors B (preys)
	  taxid of each prey, separated by ';'
	7. Experimental role A (bait)
       experimental role of protein A (should be bait or neutral)
	8. Experimental role B (preys)
       experimental role of preys (should be prey or neutral).
	9. Interaction detection method(s)
	  Interaction detection methods, taken from the corresponding PSI MI
      controlled vocabulary, and represented as PSI-MI identifier(methodName).
	10. Publication Identifier(s)
      (Pubmed) identifier(s) of the publication(s) in which this
      interaction has been shown. Database name taken from the PSI MI
      controlled vocabulary. Representation as databaseName:identifier.
      Multiple identifiers separated by ‘|’.
	11. Interaction type(s)
      Interaction type, taken from the corresponding PSI MI controlled
      vocabulary, and represented as PSI-MI identifier(methodName).	
	12. Interaction identifier(s)
		mint:mint_id
	13. bioSource_taxid	
	14. negative
	    if set to true, the interaction has been shown not to occurs
	15. mint protein group A  (taxon)
	    taxonomy group assigned by MINT for bait
	16. mint protein group B (taxon)
	    taxonomy group assigned by MINT for preys	
	17. MINT confidence score
	this score depends on all entries in MINT database. It is assigned as following:
	   1.  Interactions with only one evidence:
	   2. Interactions verified by at least two experiments:
	   3. Interactions described in a least two papers
	   4. two papers, one of the experiment is co-ip
	   5. two papers, one of the experiment is co-ip, endogenous level
	   