9606	4549	RNR1	-	MTRNR1	MIM:561000|HGNC:HGNC:7470	MT	-	s-rRNA	rRNA	MT-RNR1	mitochondrially encoded 12S RNA	O	-	20150317
9606	6052	RNR1	-	-	MIM:180450|HGNC:HGNC:10082	13	13p12	RNA, ribosomal cluster 1	other	RNR1	RNA, ribosomal cluster 1	O	RNA, ribosomal 1|Ribosomal RNA-1	20150130

___________________________________
___________________________________
___________________________________

RNR1 => 4549  |  RNR1 => 6052
RNR2 => 4550  |  RNR2 => 6053
MEMO1 => 7795  |  MEMO1 => 51072
TEC => 7006  |  TEC => 100124696
HBD => 3045  |  HBD => 100187828
MMD2 => 221938  |  MMD2 => 100505381
DEL11P13 => 100528024  |  DEL11P13 => 107648861
TRNAG-CCC => 107985600  |  TRNAG-CCC => 107985603
TRNAG-CCC => 107985603  |  TRNAG-CCC => 107985604
TRNAK-CUU => 107984908  |  TRNAK-CUU => 107985605
TRNAG-CCC => 107985604  |  TRNAG-CCC => 107985607
TRNAN-GUU => 107985601  |  TRNAN-GUU => 107985609
TRNAN-GUU => 107985609  |  TRNAN-GUU => 107985610
TRNAN-GUU => 107985610  |  TRNAN-GUU => 107985611
TRNAV-CAC => 107985608  |  TRNAV-CAC => 107985612
TRNAN-GUU => 107985611  |  TRNAN-GUU => 107985613
TRNAV-CAC => 107985612  |  TRNAV-CAC => 107985614
TRNAV-CAC => 107985614  |  TRNAV-CAC => 107985615
TRNAN-GUU => 107985613  |  TRNAN-GUU => 107985617
TRNAG-GCC => 107985618  |  TRNAG-GCC => 107985619
TRNAG-GCC => 107985619  |  TRNAG-GCC => 107985620
TRNAN-GUU => 107985617  |  TRNAN-GUU => 107985621
TRNAN-GUU => 107985621  |  TRNAN-GUU => 107985622
TRNAE-UUC => 107985602  |  TRNAE-UUC => 107985623
TRNAN-GUU => 107985622  |  TRNAN-GUU => 107985624
TRNAN-GUU => 107985624  |  TRNAN-GUU => 107985625
TRNAE-UUC => 107985623  |  TRNAE-UUC => 107985626
TRNAG-CCC => 107985607  |  TRNAG-CCC => 107985748
TRNAV-CAC => 107985615  |  TRNAV-CAC => 107985749
TRNAN-GUU => 107985625  |  TRNAN-GUU => 107985750
TRNAN-GUU => 107985750  |  TRNAN-GUU => 107985751
TRNAN-GUU => 107985751  |  TRNAN-GUU => 107985752
TRNAV-CAC => 107985749  |  TRNAV-CAC => 107985753
TRNAN-GUU => 107985752  |  TRNAN-GUU => 107985754
TRNAQ-CUG => 107985616  |  TRNAQ-CUG => 107985755
TRNAN-GUU => 107985754  |  TRNAN-GUU => 107985756
TRNAN-GUU => 107985756  |  TRNAN-GUU => 107985757
TRNAE-UUC => 107985626  |  TRNAE-UUC => 107985758
TRNAG-CCC => 107985748  |  TRNAG-CCC => 107985759
TRNAK-CUU => 107985605  |  TRNAK-CUU => 107985760
TRNAC-GCA => 107985606  |  TRNAC-GCA => 107985761
TRNAN-GUU => 107985757  |  TRNAN-GUU => 107985762
TRNAN-GUU => 107985762  |  TRNAN-GUU => 107985763
TRNAM-CAU => 107986680  |  TRNAM-CAU => 107986682
TRNAI-AAU => 107986679  |  TRNAI-AAU => 107986683
TRNAN-GUU => 107985763  |  TRNAN-GUU => 107987367
TRNAE-UUC => 107985758  |  TRNAE-UUC => 107987368
TRNAL-CAA => 107986681  |  TRNAL-CAA => 107987402
TRNAL-CAA => 107987402  |  TRNAL-CAA => 107987430
TRNAL-CAA => 107987430  |  TRNAL-CAA => 107987450
TRNAL-CAA => 107987450  |  TRNAL-CAA => 107987455
TRNAL-CAA => 107987455  |  TRNAL-CAA => 107987460
TRNAL-CAA => 107987460  |  TRNAL-CAA => 107987523
RNA5-8SP => 110467518  |  RNA5-8SP => 110467520
RNA5-8SP => 110467520  |  RNA5-8SP => 110467521
RNA5-8SP => 110467521  |  RNA5-8SP => 110467522
RNA5-8SP => 110467522  |  RNA5-8SP => 110467523
RNA5-8SP => 110467523  |  RNA5-8SP => 110467524
RNA28SP => 110467525  |  RNA28SP => 110467526
RNA28SP => 110467526  |  RNA28SP => 110467527
RNA28SP => 110467527  |  RNA28SP => 110467528
RNA28SP => 110467528  |  RNA28SP => 110467529
RNA28SP => 110467529  |  RNA28SP => 110467531
RNA18SP => 110467532  |  RNA18SP => 110467533
RNA18SP => 110467533  |  RNA18SP => 110467534
RNA18SP => 110467534  |  RNA18SP => 110467535
RNA18SP => 110467535  |  RNA18SP => 110467536
RNA18SP => 110467536  |  RNA18SP => 110467537
RNA18SP => 110467537  |  RNA18SP => 110467538
ND6 => 4541  |  ND6 => 6775063
CYTB => 4519  |  CYTB => 6775065
ND5 => 4540  |  ND5 => 6775066
ND4L => 4539  |  ND4L => 6775071
ND4 => 4538  |  ND4 => 6775072
COX3 => 4514  |  COX3 => 6775073
ATP6 => 4508  |  ATP6 => 6775074
ND3 => 4537  |  ND3 => 6775076
ATP8 => 4509  |  ATP8 => 6775077
COX2 => 4513  |  COX2 => 6775079
COX1 => 4512  |  COX1 => 6775083
ND2 => 4536  |  ND2 => 6775094
ND1 => 4535  |  ND1 => 6775098
COX3 => 6775073  |  COX3 => 8923187
ATP6 => 6775074  |  ATP6 => 8923188
ND4 => 6775072  |  ND4 => 8923197
ND6 => 6775063  |  ND6 => 8923198
ND4L => 6775071  |  ND4L => 8923201
COX2 => 6775079  |  COX2 => 8923202
CYTB => 6775065  |  CYTB => 8923205
ND3 => 6775076  |  ND3 => 8923206
ND1 => 6775098  |  ND1 => 8923209
ND5 => 6775066  |  ND5 => 8923210
ATP8 => 6775077  |  ATP8 => 8923211
ND2 => 6775094  |  ND2 => 8923212
COX1 => 6775083  |  COX1 => 8923218


___________________________________
___________________________________


checar 6118 RPA2 84172

In [12]: MSymbol['RPA2']
Out[12]: 84172

In [13]: MGeneid[6118]
Out[13]: 'RPA2'

9606	84172	POLR1B	-	RPA135|RPA2|Rpo1-2	MIM:602000|HGNC:HGNC:20454|Ensembl:ENSG00000125630
___________________________________
___________________________________
___________________________________
___________________________________
___________________________________



BIOGRID PPI file downloaded from:
https://downloads.thebiogrid.org/BioGRID/Release-Archive/BIOGRID-3.4.157/

___________________________________


BioGRID TAB 2.0 Formatted Downloads

The BioGRID TAB 2.0 format is a custom BioGRID format that is designed to provide the entirety of our curation efforst in a format that is simple to read in Microsoft Excel or to script into Bioinformatics applications. All Columns are separated by tabulations.

Back to BioGRID Download Formats
How to Detect a BioGRID TAB 2.0 file

BioGRID Tab 2.0 files are denoted by the extension .tab2.txt or .tab2.zip
Header Definitions

The first line of a BioGRID Tab 2.0 file is the heading line and starts with a hash (#). This line is purely for informational purposes and gives a brief description of the content contained in each column. If you are scripting the use of this file, you can simply ignore it.


(*) Interaction File Column Definitions

The column contents of BioGRID Interaction Tab 2.0 files should be as follows:

1    BioGRID Interaction ID. A unique identifier for each interaction within the BioGRID database. Can be used to link to BioGRID interaction pages. For example: http://thebiogrid.org/interaction/616539
2    Entrez Gene ID for Interactor A. The identifier from the Entrez-Gene database that corresponds to Interactor A. If no Entrez Gene ID is available, this will be a “-”.
3    Entrez Gene ID for Interactor B. Same structure as column 2.
4    BioGRID ID for Interactor A. The identifier in the BioGRID database that corresponds to Interactor A. These identifiers are best used for creating links to the BioGRID from your own websites or applications. To link to a page within our site, simply append the URL: http://thebiogrid.org/ID/ to each ID. For example, http://thebiogrid.org/31623/.
5    BioGRID ID for Interactor B. Same structure as column 4.
6    Systematic name for Interactor A. A plain text systematic name if known for interactor A. Will be a “-” if no name is available.
7    Systematic name for Interactor B. Same structure as column 6.
8    Official symbol for Interactor A. A common gene name/official symbol for interactor A. Will be a “-” if no name is available.
9    Official symbol for Interactor B. Same structure as column 8.
10    Synonyms/Aliases for Interactor A. A “|” separated list of alternate identifiers for interactor A. Will be “-” if no aliases are available.
11    Synonyms/Aliases for Interactor B. Same stucture as column 10.
12    Experimental System Name. One of the many Experimental Evidence Codes supported by the BioGRID.
13    Experimental System Type. This will be either “physical” or “genetic” as a classification of the Experimental System Name.
14    First author surname of the publication in which the interaction has been shown, optionally followed by additional indicators, e.g. Stephenson A (2005)
15    Pubmed ID of the publication in which the interaction has been shown.
16    Organism ID for Interactor A. This is the NCBI Taxonomy ID for Interactor A.
17    Organism ID for Interactor B. Same structure as 16.
18    Interaction Throughput. This will be either High Throughput, Low Throughput or Both (separated by “|”).
19    Quantitative Score. This will be a positive for negative value recorded by the original publication depicting P-Values, Confidence Score, SGA Score, etc. Will be “-” if no score is reported.
20    Post Translational Modification. For any Biochemical Activity experiments, this field will be filled with the associated post translational modification. Will be “-” if no modification is reported.
21    Phenotypes. If any phenotype info is recorded, it will be provided here separated by “|”. Each phenotype will be of the format <phenotype>[<phenotype qualifier>]:<phenotype type>. Note that the phenotype types and qualifiers are optional and will only be present where recorded. Phenotypes may also have multiple qualifiers in which case unique qualifiers will be separated by carat (^). If no phenotype information is available, this field will contain “-”.
22    Qualifications. If additional plain text information was recorded for an interaction, it will be listed with unique qualifiers separated by “|”. If no qualification is available, this field will contain “-”.
23    Tags. If an interaction has been tagged with additional classifications, they will be provided in this column separated by “|”. If no tag information is available, this field will contain “-”.
24    Source Database. This field will contain the name of the database in which this interaction was provided.

All columns are mandatory so columns with no values are filled with “-“
Complex File Column Definitions

___________________________________


The column contents of BioGRID Interaction Complex Tab 2.0 files should be as follows:

    BioGRID Complex ID. A unique identifier for each complex within the BioGRID database. It can appear more than once in this file as each participant in the complex is given a separate row, so all rows with the same value in this column are participants in the same complex.
    Entrez Gene ID. The identifier from the Entrez-Gene database that corresponds to the participant. If no Entrez Gene ID is available, this will be a ”-”.
    BioGRID ID. The identifier in the BioGRID database that corresponds to the participant. These identifiers are best used for creating links to the BioGRID from your own websites or applications. To link to a page within our site, simply append the URL: http://thebiogrid.org/ID/ to each ID. For example, http://thebiogrid.org/31623/.
    Systematic name. A plain text systematic name if known for the participant. Will be a “-” if no name is available.
    Official symbol. A common gene name/official symbol for the participant. Will be a “-” if no name is available.
    Synonyms/Aliases. A “|” separated list of alternate identifiers for the participant. Will be “-” if no aliases are available.
    Experimental System Name. One of the many Experimental Evidence Codes supported by the BioGRID.
    Experimental System Type. This will be either “physical” or “genetic” as a classification of the Experimental System Name.
    First author surname of the publication in which the interaction has been shown, optionally followed by additional indicators, e.g. Stephenson A (2005)
    Pubmed ID of the publication in which the interaction has been shown.
    BioGRID Publication ID. The identifier used within the BioGRID to reference this publication. These are best used for linking to publication pages such as http://thebiogrid.org/101382/publication
    Organism ID. This is the NCBI Taxonomy ID for the participant.
    Organism Name. This is the official organism name for the participant.
    Interaction Throughput. This will be either High Throughput, Low Throughput or Both (separated by “|”).
    Quantitative Score. This will be a positive for negative value recorded by the original publication depicting P-Values, Confidence Score, SGA Score, etc. Will be “-” if no score is reported.
    Post Translational Modification. For any Biochemical Activity experiments, this field will be filled with the associated post translational modification. Will be “-” if no modification is reported.
    Ontology Class. If ontology terms are mapped to this complex, the class of those terms will be listed here. If more than one term is mapped, each of their classes will be separated by a “|”.
    Ontology Terms. If ontology terms are mapped to this complex, they will be listed here separated by “|”.
    Ontology Qualifiers. If additional qualifying ontology terms are listed for the ontology terms in the previous column, they will be listed here separated by “|”. If more than one for a single term is available, they will be separated by a carat (^). If no qualifiers are listed, this field will be “-” with multiple “-” separated by “|” (example: “-|-|qualifier|-”)
    Ontology Types. If any ontology types are listed, they will be separated by “|” corresponding to the correct ontology term. No types will be represented by “-”.
    Qualifications. If additional plain text information was recorded for an interaction, it will be listed with unique qualifiers separated by “|”. If no qualification is available, this field will contain “-”.
    Tags. If an interaction has been tagged with additional classifications, they will be provided in this column separated by “|”. If no tag information is available, this field will contain “-”.
    Source Database. This field will contain the name of the database in which this interaction was provided.

All columns are mandatory so columns with no values are filled with “-“

Back to BioGRID Download Formats

