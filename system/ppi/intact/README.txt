IMPORTANT NOTICE
----------------

2009-08-27: From now on, each line of intact.txt will only refer to a single interaction evidence. 
            In the past we used to aggregate all evidences for a given pair of molecules in a single 
            line, it will now appears as multiple MITAB line. APIs such as intact-psimitab 2.1.0 and
            psimitab-parser 1.7.6 do reflect this change.  


The MITAB25 format is part of the PSI-MI 2.5 standard [1].
It has been derived from the tabular format provided by BioGrid. 
MITAB25 only describes binary interactions, one pair of interactors per row. 
Columns are separated by tabulations.

FORMAT
------

The column contents should be as follows:

1.  Unique identifier for interactor A, represented as databaseName:ac, where 
    databaseName is the name of the corresponding database as defined in the 
    PSI-MI controlled vocabulary, and ac is the unique primary identifier of 
    the molecule in the database. Identifiers from multiple databases can be 
    separated by "|". It is recommended that proteins be identified by stable 
    identifiers such as their UniProtKB or RefSeq accession number.
2.  Unique identifier for interactor B.
3.  Alternative identifier for interactor A, for example the official gene 
    symbol as defined by a recognised nomenclature committee. Representation 
    as databaseName:identifier. Multiple identifiers separated by "|". 
4.  Alternative identifier for interactor B.
5.  Aliases for A, separated by "|". Representation as databaseName:identifier. 
    Multiple identifiers separated by "|". 
6.  Aliases for B.
7.  Interaction detection methods, taken from the corresponding PSI-MI controlled 
    vocabulary, and represented as darabaseName:identifier(methodName), 
    separated by "|".
8.  First author surname(s) of the publication(s) in which this interaction has 
    been shown, optionally followed by additional indicators, e.g. "Doe-2005-a". 
    Separated by "|".
9.  Identifier of the publication in which this interaction has been shown. 
    Database name taken from the PSI-MI controlled vocabulary, represented as 
    databaseName:identifier. Multiple identifiers separated by "|".
10. NCBI Taxonomy identifier for interactor A. Database name for NCBI taxid taken 
    from the PSI-MI controlled vocabulary, represented as databaseName:identifier. 
    Multiple identifiers separated by "|". 
    Note: In this column, the databaseName:identifier(speciesName) notation is only 
    there for consistency. Currently no taxonomy identifiers other than NCBI taxid are 
    anticipated, apart from the use of -1 to indicate "in vitro" and -2 to indicate 
    "chemical synthesis".
11. NCBI Taxonomy identifier for interactor B.
12. Interaction types, taken from the corresponding PSI-MI controlled vocabulary, and 
    represented as dataBaseName:identifier(interactionType), separated by "|".
13. Source databases and identifiers, taken from the corresponding PSI-MI controlled 
    vocabulary, and represented as databaseName:identifier(sourceName). Multiple source 
    databases can be separated by "|".
14. Interaction identifier(s) in the corresponding source database, represented by 
    databaseName:identifier
15. Confidence score. Denoted as scoreType:value. There are many different types of 
    confidence score, but so far no controlled vocabulary. Thus the only current 
    recommendation is to use score types consistently within one source. 
    Multiple scores separated by "|".

All columns are mandatory.



IntAct has added 11 additional columns to this format in order to accomodate the needs of the user community:

16. Experimental role(s) of interactor A, taken from the corresponding PSI-MI controlled vocabulary, and 
    represented as dataBaseName:identifier(role). Multiple roles separated by "|".
17. Experimental role(s) of interactor B, taken from the corresponding PSI-MI controlled vocabulary, and 
    represented as dataBaseName:identifier(role). Multiple roles separated by "|".
18. Biological role(s) of interactor A, taken from the corresponding PSI-MI controlled vocabulary, and 
    represented as dataBaseName:identifier(role). Multiple roles separated by "|".
19. Biological role(s) of interactor B, taken from the corresponding PSI-MI controlled vocabulary, and 
    represented as dataBaseName:identifier(role). Multiple roles separated by "|".
20. Properties (CrossReference) of interactor A, represented as dataBaseName:identifier(label).
    Multiple properties separated by "|".
21. Properties (CrossReference) of interactor A, represented as dataBaseName:identifier(label)
    Multiple properties separated by "|".
22. Molecule Type of interactor A, taken from the corresponding PSI-MI controlled vocabulary, and 
    represented as dataBaseName:identifier(type)
23. Molecule Type of interactor B, taken from the corresponding PSI-MI controlled vocabulary, and 
    represented as dataBaseName:identifier(type)
24. NCBI Taxonomy identifier of the host organism in which the experiment was carried out.
    Multiple organisms separated by "|".
25. Expansion method(s) should the binary interaction have been produced computationaly from a complex.
    If the field is empty, it is safe to assume that a binary interaction was reported in the corresponding 
    publication. Multiple methods separated by "|".
26. Dataset name(s) with which this interaction has been tagged.
    Multiple datasets separated by "|".
27. Annotation(s) of interactor A, are free text values that have been annotated under a specific annotation topic.
    Represented as type:text
    Multiple organisms separated by "|".
28. Annotation(s) interactor B , are free text values that have been annotated under a specific annotation topic.
    Represented as type:text
    Multiple organisms separated by "|".
29. Parameter(s) interactor A, are numerical parameter reported during the experimental process.
    Represented as type:numerical_value(unit).
    Multiple organisms separated by "|".
30. Parameter(s) interactor B, are numerical parameter reported during the experimental process.
    Represented as type:numerical_value(unit).
    Multiple organisms separated by "|".
31. Parameter(s) interaction, are numerical parameter reported during the experimental process.
    Represented as type:numerical_value(unit).
    Multiple organisms separated by "|".

Where values are missing, an empty cell is marked by "-".


SOFTWARE
--------

The PSI-MI workgroup has developed a parser for PSIMITAB, more information can be found on the PSI web site [2].
Should you want to parse the extra columns IntAct is providing, you can use the Java library we have developed for that purpose.
Currently we only provide it as a Maven 2 dependency (see below), should you need more information about it, contact us [3]

	<dependency>
		<groupId>uk.ac.ebi.intact.dataexchange.psimi</groupId>
		<artifactId>intact-psimitab</artifactId>
		<version>2.0.0</version>
	</dependency>

you can also download the source code that is available on subversion:

	https://intact.svn.sourceforge.net/svnroot/intact/repo/data-exchange/trunk/psimi/psimitab/intact-psimitab


REFERENCES
----------

[1] http://www.pubmedcentral.nih.gov/articlerender.fcgi?tool=pubmed&pubmedid=17925023
[2] http://www.psidev.info/index.php?q=node/60#tools
[3] http://www.ebi.ac.uk/support/index.php?query=intact
