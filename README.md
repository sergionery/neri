
## Network-medicine by Relative Importance
Paper: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4686785/

---
## How to install



We strongly recommend using anaconda to install the necessary libraries.

- [Install anaconda](https://docs.anaconda.com/anaconda/install/)

And then, create an enviroment.

```
conda create -n neri matplotlib pandas scipy networkx plotly

conda activate neri

conda install -c conda-forge kivy
```

Whenever you want to use, activate the environment with the following command:

```
conda activate neri
```

And run: 

```
python neri.py
```

Or, if you prefer, you can install the libraries manually.

1. Click git clone or Download repository.
2. Install Python (>= 2.7) with package manager pip
3. Inside NERI's folder, open a terminal and use this command to install requirements "pip install -r requirements.txt"
4. Install python-scipy (on Linux: sudo apt install python-scipy)
5. Install python-tk (on Linux: sudo apt install python-tk)
6. Choose your platform to install kivy library 
    - [Install on Linux](https://kivy.org/doc/stable/installation/installation-linux.html)
    - [Install on Windows](https://kivy.org/doc/stable/installation/installation-windows.html)
    - [Install on OS X](https://kivy.org/doc/stable/installation/installation-osx.html)
7. Then, to execute run this command "python neri.py"

---

## Tutorial CLI - Network-medicine by Relative Importance

This tutorial shows how to create a new experiment to be used by NERI (Network-medicine by Relative Importance) method by CLI (Command Line Interface). In the folder user/input there is a config.ini file, which contains all the experiments available to execution and analysis. The file has the following format:

1. [SZ_KATO2]
2. keywords = 'schizo; schizophrenia'
3. description = 'this data is about a disease that..'
4. epsilon = 0.05
5. ppi = ['hprd', 'intact', 'biogrid', 'mint']
6. file_seeds = user/input/SZ_KATO/seeds/seeds_schizophrenia.txt
7. file_expression = user/input/SZ_KATO/expression/norm_Kato_34Cx35D.txt

We highlight the line 1 which is the EXPERIMENT_ID that will be used when calling via CLI. Lines 2 and 3 have keywords and description about the experiment. Line 4 has the epsilon (value corresponding to the difference to be used as a tie). GENE SYMBOLS should be the first column in the expression file. In line 5 has a list of all PPI’s that will be used in the experiment. Lines 6 and 7 are the paths where the file of seeds and expression are in file system (OBS1: these file must be tab separated; OBS2: seeds file must be without header).

To place the information of seeds and expression, you need to create a folder with the experiment ID “user/input/<EXPERIMENT_ID>”, with the same name registered in the config.ini file. Inside the folder “user/input”, it should be created two subfolders “seeds” and “expression” with the corresponding files in each one. 

To execute:
In the terminal, go to the program NERI folder -- where there is the neri.py file -- and calls the NERI program passing the EXPERIMENT ID parameter, such as registered in the config.ini file, like this:
python neri.py EXPERIMENT_ID

The program will start to execute. At the end, inside the folder user/output, the program will create a subfolder with the Experiment ID of your experiment: “user/output/<EXPERIMENT_ID>”. This folder will contain all results, including the nodes and edges of differential network generated.

---
## Tutorial GUI - Network-Medicine by Relative Importance

The purpose of this tutorial is to provide an overview about the NERI (Network-medicine by Relative Importance) method implemented with a GUI (Graphical User Interface) and a quick introduction about its screens tabs. There are five screens:

1. creation of a new experiment, which has the necessary input fields to create the configuration file;
2. loading of the experiment, which presents a summary of the integration of the chosen databases;
3. execution, in which you can follow the progress of the program and also define the level of detail you want to follow (log);
4. analysis, in which it is possible to load the generated data in the previous step (execution) to perform a visual analysis through two graphical options, which are the power law and differential network interpolation graph;
5. help, which provides more information about the details of the system, access to the construction of the program, and articles related to the NERI method.

Below there is a more detailed view of program's steps, as well as the objectives of each one. Figure 1 shows the screen that assists the user in creating a new experiment. In this screen, the user enters the information of the experiment, which are the parameters and data files. The parameters are the experiment ID (ExpID), keywords, experiment description and epsilon parameter (which determines the maximum percentage that a shortest path score can be below the best shortest path score to be chosen as one of the best shortest paths to belong to the final network cutting). In addition, the user is required to inform the data files, which are: the (tab separated) file of genes used as seeds in the network,as well as the genes column, the expression file and a box to select PPI networks (BIOGRID, IntAct, HPRD and MINT). Once this information is configured, it requires just a click on "Create" to save the configuration file. This file will copy the data files (with seeds and gene expression) into the directory "./user/input/<ExpID>" and will create an entry in the configuration file "./user/input/config.ini”. Finally, this entry can be used in the "load" tab.

![Figure 1: Screen for creation of a new experiment](/img/new.png "Figure 1: Screen for creation of a new experiment")

Figure 2 shows the screen to assist the loading and data integration, allowing to follow the summary of the databases integration. The Experiment ID field has a dropdown box which dynamically shows all experiments already created, i.e., that are registered in the configuration file. Given the options, the user can choose one and click on "Load" to load them into memory. After the click, in the session description the user can follow the integration in real time, besides being able to choose the level of detail of the information to be displayed. The levels can be divided into: info, showing only the main steps of program execution; debug, showing all steps of the program (the most verbose option); none, which do not show anything on the screen. After the execution is finished, the data will already be ready for the next step ("Execute" tab).

![Figure 2: Screen for experiment loading (data integration)](/img/load.png "Figure 2: Screen for experiment loading (data integration)")

In Figure 3, the screen for monitoring the execution of the program is displayed. Just click "Start" to activate the Execution session log, as well as the beginning of the time marking and progress bar at the bottom of the screen. The level of detail of the information follows the same pattern as the previous screen, the user can choose between: None, debug and info, where the last is the level assigned by default. The outputs generated by this process will be stored in the “user/output/<ExpID>” directory, which can be used as inputs to carry out the analyses (performed in "Analysis" tab).

![Figura 3: Program Running Screen](/img/execute.png "Figura 3: Program Running Screen")

Figure 4 shows the analysis screen which assists the user in creating graphs from the data generated in the previous step. In this screen, the user enters the information of the experiment corresponding to the parameters and data files of nodes and edges. The parameters are the initial X (which filters noisy genes: those points presenting X bellow this minimal value will be dropped off) , the TopN (which represents the number of genes to be selected), besides having the advanced settings, which are in the panel illustrated by Figure 5, and are already pre-defined but can be modified by the user when clicking on the gear icon and opening the "Advanced Configuration" screen.

![Figure 4: Graphical Analysis Screen ](/img/analysis.png "Figure 4: Graphical Analysis Screen")

![Figura 5: Advanced configuration screen](/img/advanced.png "Figura 5: Advanced configuration screen")

After entering the nodes data and parameter settings, simply click on the buttons to generate the desired graph. By clicking on "Generate" button, a power-law interpolation graph will be generated, as seen in Figure 6. And by clicking on “Differential Network” button, it will generate a network file in GML format, which is supported by Cytoscape software. This file can be found in the “user/output/<ExpID> directory. Figure 7 shows the network visualization in Cytoscape.

![Figure 6: Power-law interpolation](/img/powerlaw.png "Figure 6: Power-law interpolation")

![Figure 7: Differential Network Visualization](/img/network.png "Figure 7: Differential Network Visualization")

Figure 8 shows the screen containing information about the program. This screen is composed of four buttons. The “About” button allows to access the article describing the NERI method; “Tutorial 1” button enables the tutorial which explains all stages of the program flow. In the “Documentation” button, there is the link to the documentation of the program itself, which includes the functions used and their descriptions. Finally, by clicking on “Contact us” button, the basic contact information for the developers is shown.

![Figure 8: Help Menu](/img/help.png "Figure 8: Help Menu")


